#!/bin/bash

set -euo pipefail

# Set default values
DOCKER_IMAGE_NAME="synthesis"
DOCKER_IMAGE_TAG="latest"

# Create output directory if non-existent
mkdir -p output

# Check if arguments are provided
if [ $# -eq 1 ]; then
    DOCKER_IMAGE_NAME=$1
fi

# Check if arguments are provided
if [ $# -eq 2 ]; then
    DOCKER_IMAGE_NAME=$1
    DOCKER_IMAGE_TAG=$2
fi

# Build the Docker image
docker build -t $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG .

# Run the Docker container
docker run -it --rm \
    -v $(pwd)/input:/opt/input \
    -v $(pwd)/output:/opt/output \
    $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG sh -c "synthesis && python3 postprocess.py"