# EoS Synthesis Module

This code provides methods for joining two one-dimensional Equations of State (EoS). Current methods available are:

- Maxwell phase transition (PT)
- Gibbs phase transition
- Hyperbolic tangent interpolation 
- Attaching the two EoS at a user defined variable and value

You can find more information about how to run the code, inputs and outputs, and changes from past versions at:  

- [Documentation](./docs/src/index.rst).

- [Changelog](./CHANGELOG.md)

- [OpenAPI Specifications](./api/OpenAPI_Specifications_synthesis.yaml)