FROM --platform=linux/amd64 python:3.11-slim as build
############################################################################################

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq \
    g++ \
    make \
    libceres-dev \
    libcgal-dev \
    libgsl-dev \
    libyaml-cpp-dev \
    libgtest-dev \
    && rm -rf /var/lib/apt/lists/*

COPY test/ /tmp/test

COPY src/ /tmp/src
    
WORKDIR /tmp/src

## Build source code
RUN make redo

RUN make build_tests
    


FROM --platform=linux/amd64 python:3.11-slim as deps
#############################
# Install git for pip install
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq \
        git \
    && rm -rf /var/lib/apt/lists/*
## Install dependencies to /usr/local/lib/python3.11/site-packages/
COPY requirements.txt /opt/
RUN pip --no-cache-dir install -r /opt/requirements.txt --no-compile


FROM --platform=linux/amd64 python:3.11-slim
#####################
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq \
    libgsl27 \
    libceres-dev \
    libyaml-cpp0.7 \
    libgtest-dev \
    make \
    && rm -rf /var/lib/apt/lists/*

ARG USERNAME=synthesis
ARG UID=1000

## Create user with desired UID and set "home" to /opt
RUN useradd --uid $UID --no-log-init --no-create-home --home-dir /opt --shell /bin/bash $USERNAME
RUN chown -R $UID:$UID /opt
USER $USERNAME


## Install compiled executable and add to PATH
ENV PATH="/opt/src:/opt/.local/bin:${PATH}"
## Install Python dependencies
COPY --from=deps --chown=$UID:$UID /usr/local/lib/python3.11/site-packages/ /opt/.local/lib/python3.11/site-packages/

## Install source code with compiled output and copy other files
WORKDIR /opt
COPY --from=build --chown=$UID:$UID /tmp/src src
COPY --from=build --chown=$UID:$UID /tmp/test test
COPY --chown=$UID:$UID api api
COPY --chown=$UID:$UID docs docs

COPY --chown=$UID:$UID manifest.yaml ./
COPY --chown=$UID:$UID Dockerfile ./
COPY --chown=$UID:$UID LICENSE ./
COPY --chown=$UID:$UID README.md ./
# COPY --chown=$UID:$UID *.sh ./


## create input and output directories
RUN mkdir input/ && chown $UID:$UID input/
RUN mkdir output/ && chown $UID:$UID output/

# change to source code directory
WORKDIR /opt/src

CMD ["/bin/bash"]
