#pragma once

#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/K_neighbor_search.h>
#include <CGAL/Search_traits_2.h>
#include <CGAL/Search_traits_3.h>

#include <cassert>
#include <queue>

#include "log.hpp"
#include "utils.hpp"

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

// Variables for 2d interpolation
typedef CGAL::Delaunay_triangulation_2<K> Delaunay_2d;
typedef K::Point_2 Point_2d;
typedef std::map<Point_2d, double> PointValueMap_2d;

// Variables for 3d interpolation
typedef CGAL::Delaunay_triangulation_3<K> Delaunay_3d;
typedef K::Point_3 Point_3d;
typedef std::map<Point_3d, double> PointValueMap_3d;

namespace Synthesis {

/**
 * @brief Interpolates a 1D function y(x) at a point x_.
 * @param x_ The x-coordinate at which to interpolate.
 * @param xv_ The vector of x-values.
 * @param func_ The corresponding vector of y-values.
 * @return The interpolated value at x_.
 */

class Interpolator_1d {
   private:
    std::vector<double> xv;
    std::vector<std::vector<double>> func;

   public:
    Interpolator_1d() = default;

    void set_1d_interpolation(const std::vector<std::vector<double>>& data, int iy = 3) {
        if (data.empty() || data[0].empty()) {
            StatusLog::getInstance().setCode(500);
            throw  std::runtime_error("Error setting up 1D interpolation. Empty data");
        }

        reset_1d_interpolation();
        xv = get_column_from_matrix(data, iy);

        // Create a vector of functions
        for (size_t i = 0; i < data[0].size(); i++) {
            func.push_back(get_column_from_matrix(data, i));
        }
    }

    double interpolate_1d(double x_, int i) const {
        if (x_ < xv.front() || x_ > xv.back()) {
            return std::numeric_limits<double>::infinity();
        }

        auto j = lower_bound(xv.begin(), xv.end(), x_);
        size_t k = j - xv.begin();  // Nearest index

        if (j == xv.end())
            --k;
        else if (*j == x_) {
            return func[i][k];
        }

        size_t l = k ? k - 1 : 1;

        double xk = xv[k];
        double xl = xv[l];
        double fk = func[i][k];
        double fl = func[i][l];

        double y_ = (xk < xl) ? fk + (x_ - xk) * (fl - fk) / (xl - xk) : fl + (x_ - xl) * (fk - fl) / (xk - xl);

        return y_;
    }

    void reset_1d_interpolation() {
        xv.clear();
        func.clear();
    }

    bool is_value_in_interpolator_1d(double x) const {
        if (xv.empty()) {
            return false;
        }

        double min_x = *std::min_element(xv.begin(), xv.end());
        double max_x = *std::max_element(xv.begin(), xv.end());

        if (x < min_x || x > max_x) {
            return false;
        }

        return true;
    }

    const std::vector<double>& get_x_vector() { return xv; }

    const std::vector<std::vector<double>>& get_func() { return func; }
};

class Interpolator_2d {
   private:
    std::vector<Point_2d> points;
    std::vector<PointValueMap_2d> function;
    Delaunay_2d dt;
    double min_x, max_x, min_y, max_y;
    std::vector<double> x_vector, y_vector;

   public:
    Interpolator_2d() = default;

    void set_2d_interpolation(const std::vector<std::vector<double>>& data, int ix = 1, int iy = 3) {
        reset_2d_interpolation();

        if (data.empty() || data[0].empty()) {
            StatusLog::getInstance().setCode(500);
            throw  std::runtime_error("Error setting up 1D interpolation. Empty data");
        }

        points.clear();

        x_vector = get_column_from_matrix(data, ix);
        y_vector = get_column_from_matrix(data, iy);

        min_x = *std::min_element(x_vector.begin(), x_vector.end());
        max_x = *std::max_element(x_vector.begin(), x_vector.end());
        min_y = *std::min_element(y_vector.begin(), y_vector.end());
        max_y = *std::max_element(y_vector.begin(), y_vector.end());

        // Define a vector with all the interpolation points (muB, muQ)
        for (size_t i = 0; i < data.size(); i++) {
            points.push_back(
                Point_2d((data[i][ix] - min_x) / (max_x - min_x), (data[i][iy] - min_y) / (max_y - min_y)));
        };
        // Build triangulation with the vector of points
        dt.insert(points.begin(), points.end());

        // Associate a function to these points:
        for (size_t i = 0; i < data[0].size(); i++) {
            PointValueMap_2d function_of_points;
            for (size_t j = 0; j < data.size(); j++) {
                function_of_points[Point_2d((data[j][ix] - min_x) / (max_x - min_x),
                                            (data[j][iy] - min_y) / (max_y - min_y))] = data[j][i];
            }
            function.push_back(function_of_points);
        }
    }

    /**
     * @brief Performs 2D interpolation at a given point (x_, y_).
     * @param x_ The x-coordinate of the point.
     * @param y_ The y-coordinate of the point.
     * @param index_ Index specifying which dataset to use for interpolation.
     * @return The interpolated value at the point (x_, y_).
     */
    double interpolate_2d(double x_, double y_, int index_) const {
        Point_2d p((x_ - min_x) / (max_x - min_x), (y_ - min_y) / (max_y - min_y));

        // locate the point in the triangulation
        Delaunay_2d::Face_handle face = dt.locate(p);

        if (dt.is_infinite(face)) {
            // Handle points outside the triangulation
            return std::numeric_limits<double>::infinity();
        }

        Point_2d a = face->vertex(0)->point();
        Point_2d b = face->vertex(1)->point();
        Point_2d c = face->vertex(2)->point();

        double wa = CGAL::area(p, b, c) / CGAL::area(a, b, c);
        double wb = CGAL::area(a, p, c) / CGAL::area(a, b, c);
        double wc = CGAL::area(a, b, p) / CGAL::area(a, b, c);

        return wa * function[index_].at(a) + wb * function[index_].at(b) + wc * function[index_].at(c);
    }

    void reset_2d_interpolation() {
        points.clear();
        function.clear();
        dt.clear();
    }

    bool is_value_in_interpolator_2d(double x_, double y_) const {
        Point_2d query_point((x_ - min_x) / (max_x - min_x), (y_ - min_y) / (max_y - min_y));
        auto face_handle = dt.locate(query_point);

        return !dt.is_infinite(face_handle);
    }

    const std::vector<Point_2d>& get_points_2d() { return points; }

    const std::vector<PointValueMap_2d>& get_function_2d() { return function; }
};

class Interpolator_3d {
   private:
    std::vector<Point_3d> points;
    std::vector<PointValueMap_3d> function;
    Delaunay_3d dt;

   public:
    Interpolator_3d() = default;

    void set_3d_interpolation(std::vector<std::vector<double>> data, int ix = 1, int iy = 2, int iz = 3) {
        reset_3d_interpolation();
        if (data.empty() || data[0].empty()) {
            StatusLog::getInstance().setCode(500);
            throw  std::runtime_error("Error setting up 1D interpolation. Empty data");
        }

        for (size_t i = 0; i < data.size(); i++) {
            points.push_back(Point_3d(data[i][ix], data[i][iy], data[i][iz]));
        };

        // Build triangulation with the vector of points
        dt.insert(points.begin(), points.end());

        // Associate a function to these points:
        for (size_t i = 0; i < data[0].size(); i++) {
            PointValueMap_3d function_of_points;
            for (size_t j = 0; j < data.size(); j++) {
                function_of_points[Point_3d(data[j][ix], data[j][iy], data[j][iz])] = data[j][i];
            }
            function.push_back(function_of_points);
        }
    }

    double interpolate_3d(double x_, double y_, double z_, int index_) const {
        Point_3d p(x_, y_, z_);

        // locate the point in the triangulation
        Delaunay_3d::Cell_handle cell = dt.locate(p);

        if (dt.is_infinite(cell)) {
            // Handle points outside the triangulation
            return std::numeric_limits<double>::infinity();
            ;  // or any sentinel/error value
        }

        Point_3d a = cell->vertex(0)->point();
        Point_3d b = cell->vertex(1)->point();
        Point_3d c = cell->vertex(2)->point();
        Point_3d d = cell->vertex(3)->point();
        double volume = CGAL::volume(a, b, c, d);
        double wa = CGAL::volume(p, b, c, d) / volume;
        double wb = CGAL::volume(a, p, c, d) / volume;
        double wc = CGAL::volume(a, b, p, d) / volume;
        double wd = CGAL::volume(a, b, c, p) / volume;

        return wa * function[index_].at(a) + wb * function[index_].at(b) + wc * function[index_].at(c) +
               wd * function[index_].at(d);
    }

    void reset_3d_interpolation() {
        points.clear();
        function.clear();
        dt.clear();
    }

    bool is_value_in_interpolator_3d(double x, double y, double z) const {
        Point_3d query_point(x, y, z);
        auto cell_handle = dt.locate(query_point);
        return !dt.is_infinite(cell_handle);
    }

    const std::vector<Point_3d>& get_points_3d() { return points; }

    const std::vector<PointValueMap_3d>& get_function_3d() { return function; }
};

class Interpolator : public Interpolator_1d, public Interpolator_2d, public Interpolator_3d {
   private:
    int dimension = 0;

   public:
    Interpolator() {}

    Interpolator(const Interpolator& other)
        : Interpolator_1d(other), Interpolator_2d(other), Interpolator_3d(other), dimension(other.dimension) {}

    Interpolator(Interpolator&& other) noexcept
        : Interpolator_1d(std::move(other)),
          Interpolator_2d(std::move(other)),
          Interpolator_3d(std::move(other)),
          dimension(std::exchange(other.dimension, 0)) {}

    Interpolator& operator=(const Interpolator& other) {
        if (this != &other) {
            Interpolator_1d::operator=(other);
            Interpolator_2d::operator=(other);
            Interpolator_3d::operator=(other);
            dimension = other.dimension;
        }
        return *this;
    }

    double operator()(int i, double x_, std::optional<double> y_ = std::nullopt,
                      std::optional<double> z_ = std::nullopt) const {

        if (y_.has_value() && z_.has_value()) {
            return interpolate_3d(x_, y_.value(), z_.value(), i);
        } else if (y_.has_value() && !z_.has_value()) {
            return interpolate_2d(x_, y_.value(), i);
        } else if (!y_.has_value() && z_.has_value()) {
            return interpolate_2d(x_, z_.value(), i);
        } else {
            return interpolate_1d(x_, i);
        }
    }

    void set_interpolator(std::vector<std::vector<double>> data, int ix, std::optional<int> iy = std::nullopt,
                          std::optional<int> iz = std::nullopt) {
        if (iy.has_value() && iz.has_value()) {
            set_3d_interpolation(data, ix, iy.value(), iz.value());
            dimension = 3;
        } else if (iy.has_value() && !iz.has_value()) {
            set_2d_interpolation(data, ix, iy.value());
            dimension = 2;
        } else if (!iy.has_value() && iz.has_value()) {
            set_2d_interpolation(data, ix, iz.value());
            dimension = -2;
        } else {
            set_1d_interpolation(data, ix);
            dimension = 1;
        }
    }

    void clear() {
        reset_1d_interpolation();
        reset_2d_interpolation();
        reset_3d_interpolation();
        dimension = 0;
    }

    bool is_value_in_interpolator(double x, std::optional<double> y = std::nullopt,
                                  std::optional<double> z = std::nullopt) const {
        switch (dimension) {
            case 3:
                return is_value_in_interpolator_3d(x, y.value(), z.value());
            case 2:
                return is_value_in_interpolator_2d(x, y.value());
            case -2:
                return is_value_in_interpolator_2d(x, z.value());
            case 1:
                return is_value_in_interpolator_1d(x);
            default:
                return false;
        }
    }
};

}  // namespace Synthesis

// End utilities