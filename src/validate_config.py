#!/usr/bin/env python
# -*- coding: utf-8 -*-

## Validate the config.yaml to ensure that it meets the OpenAPI specifications
## If any fields are left empty, the config.yaml will be overwritten with these fields filled by default values

import argparse
import os
import yaml
import json
from openapi_core import Spec
from openapi_core import unmarshal_request
from openapi_core.testing import MockRequest

# Constants
DEFAULT_API_FILE_PATH = os.path.join(os.path.dirname(__file__), '..', 'api/OpenAPI_Specifications_synthesis.yaml')
DEFAULT_CONFIG_FILE_PATH = os.path.join(os.path.dirname(__file__), '..', 'input')
DEFAULT_CONFIG_FILE= "config.yaml"


def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="Validate and unmarshal YAML configuration file for Chiral EFT module based on the OpenAPI specification"
    )

    # Paths
    parser.add_argument(
        "--api_file_path",
        type=str,
        default=DEFAULT_API_FILE_PATH,
        help="Path to the OpenAPI specification file",
    )

    parser.add_argument(
        "--config_file_path",
        type=str,
        default=DEFAULT_CONFIG_FILE_PATH,
        help="Path to the initial configuration file",
    )

    parser.add_argument(
        "--output_config_file_path",
        type=str,
        default=DEFAULT_CONFIG_FILE_PATH,
        help="Path to the final validated configuration file",
    )

    args = parser.parse_args()

    # Validate input config file against OpenAPI spec and create validated config
    assert validate_unmarshal_file(spec_file_path=args.api_file_path, input_file_path=os.path.join(args.config_file_path, DEFAULT_CONFIG_FILE), valid_file_path=os.path.join(args.output_config_file_path, DEFAULT_CONFIG_FILE))

def validate_unmarshal_file(spec_file_path, input_file_path, valid_file_path, verbose=True):
    # Reading OpenAPI specifications
    with open(spec_file_path, 'r') as fp:
        openapi_specs = Spec.from_file(fp)
    
    # Reading input file
    with open(input_file_path, 'r') as fp:
        data = yaml.safe_load(fp)
    
    # Define request
    request = MockRequest(
        host_url='/',
        method="PUT",
        path='/input/config.yaml',
        data=json.dumps(data)
    )

    # Validate and unmarshal request
    try:
        valid_result = unmarshal_request(spec=openapi_specs, request=request)
        
    except Exception as err:

        if verbose:
            print(f'''Invalid config file "{input_file_path}" against OpenAPI spec file "{spec_file_path}":''')
            errors = f'''{err}'''
            for error in err.__cause__.schema_errors:
                if error.message != "None for not nullable":
                    if len(error.relative_path) != 0:

                        for i in range(len(error.relative_path)):
                            if i == 0:
                                errors += f" ['{error.relative_path[i]}']"
                            else:
                                errors += f"['{error.relative_path[i]}']"
                        errors += " -> "+ error.message
                    else:
                        errors += " -> "+ error.message
            print(f'''{errors}''')

        status_data = {
        "code": 400,
        "message": f'Invalid input. {errors}'
        }

        # Write the error details to status.yaml
        with open("../output/status.yaml", "w") as file:
            yaml.dump(status_data, file, default_flow_style=False)
    
        return False
    
    
    with open(valid_file_path, 'w') as fp:
        yaml.safe_dump(valid_result.body, fp)
    return True


if __name__ == "__main__":
    print("\nStarting execution of yaml_validation.py...")
    main()
    print("\nFinished yaml_validation.py...")