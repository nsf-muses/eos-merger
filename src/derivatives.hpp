#pragma once

#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>
#include <gsl/gsl_interp.h>	
#include <gsl/gsl_spline.h>
#include <queue>

#include "input.hpp"
#include "interpolator.hpp"
#include "data.hpp"

namespace Synthesis {

class DerivativeStrategy {
protected:
    DataContainer<double>& data;
    InputParams& input;

    std::string x_variable;
    virtual void initialize_interpolator()=0;

public:
    DerivativeStrategy(DataContainer<double>& data_, InputParams& input_) : data(data_), input(input_) {
        x_variable = "";
    }

    virtual ~DerivativeStrategy() = default;

    virtual double first_derivative( const std::string &y_variable, double x) const = 0;
    virtual double second_derivative(const std::string &y_variable, double x) const = 0;
    virtual double third_derivative( const std::string &y_variable, double x) const = 0;

    virtual void set_x_variable(const std::string& variable) = 0;
    virtual void set_y_variable(const std::string& variable){
        (void) variable;
    };
};


class FiniteDifferenceDerivative : public DerivativeStrategy {
private:

protected:

    void initialize_interpolator() {
        
        data.order_in(x_variable);
        data.setup_interpolator_in(x_variable);
    }

    double get_step(double x) const{
        return (input.step_type == "absolute" ? input.step_size : input.step_size * x);
    }
public:

    FiniteDifferenceDerivative(DataContainer<double>& data_, InputParams& input_) : DerivativeStrategy(data_, input_) {}

    ~FiniteDifferenceDerivative()=default;


    // First-order derivative using central difference formula to first order in h
    double first_derivative(const std::string &y_variable, double x) const override{
        double h=get_step(x);
        switch (input.precision) {
            case 1:
                return (data.interpolate(y_variable, x + h) - data.interpolate(y_variable, x-h)) /
                       (2. * h);
            case 2:
                return (   -data.interpolate(y_variable, x + 2 * h) +
                        8 * data.interpolate(y_variable, x + h) -
                        8 * data.interpolate(y_variable, x - h) +
                            data.interpolate(y_variable, x - 2. * h) )/
                       (12. * h);
            default:
                std::cerr << "Invalid precision order: " << input.precision <<  std::endl;
                return 0.0;
        }
    }

    // Second-order derivative using central difference formula to first order in
    double second_derivative(const std::string &y_variable, double x) const override{
        double h=get_step(x);
        switch (input.precision) {
            case 1:
                return (data.interpolate(y_variable, x+ h) - 2. * data.interpolate(y_variable, x) +
                        data.interpolate(y_variable, x -h )) /
                       (std::pow(x * h, 2));
            case 2:
                return (-data.interpolate(y_variable, x + 2.*h) +
                        16. * data.interpolate(y_variable, x + h) - 30. * data.interpolate(y_variable, x) +
                        16. * data.interpolate(y_variable, x  - h) -
                        data.interpolate(y_variable, x - 2.*h)) /
                       (12. * std::pow(h, 2));
            default:
                std::cerr << "Invalid precision order: " << input.precision <<  std::endl;
                return 0.0;
        }
    }

    // Third-order derivative using central difference formula to first order in
    double third_derivative(const std::string &y_variable, double x) const override{
        double h=get_step(x);
        switch (input.precision) {
            case 1:
                return (data.interpolate(y_variable, x+ 2.*h) -
                        2 * data.interpolate(y_variable, x + h) +
                        2 * data.interpolate(y_variable, x  - h) -
                        data.interpolate(y_variable, x - 2.*h)) /
                       (2 * std::pow(h, 3));
            case 2:
                return (-data.interpolate(y_variable, x  + 3 * h) +
                        8 * data.interpolate(y_variable, x + 2 * h) -
                        13 * data.interpolate(y_variable, x + h) +
                        13 * data.interpolate(y_variable, x  - h) -
                        8 * data.interpolate(y_variable, x  - 2 * h) +
                        data.interpolate(y_variable, x- 3 * h) )/
                       (8 * std::pow(h, 3));
            default:
                std::cerr << "Invalid precision order: " << input.precision <<  std::endl;
                return 0.0;
        }
    }

    void set_x_variable(const std::string& variable) override{
        x_variable = variable;

        if(data.get_interpolator_variables().empty()){
            initialize_interpolator();   

        }else if(x_variable != data.get_interpolator_variables()[0]){
            initialize_interpolator();   
        }
            
    }
    
};


class GSLDerivative : public DerivativeStrategy {
private:
    std::vector<double> xv, yv;
    std::string saved_y_variable;
    std::string saved_x_variable;

    gsl_interp_accel* acc;
    gsl_interp* interpolation;

protected:

    void initialize_interpolator() override {
        if (interpolation) gsl_interp_free(interpolation);
        interpolation = gsl_interp_alloc(gsl_interp_cspline, xv.size());
        
        gsl_interp_init(interpolation, xv.data(), yv.data(), xv.size());
    }

public:

    GSLDerivative(DataContainer<double>& data_, InputParams&input_) : DerivativeStrategy(data_, input_) {
        interpolation = nullptr; 
        acc = gsl_interp_accel_alloc();
    }

    ~GSLDerivative() {
        gsl_interp_accel_free(acc);
        if (interpolation) gsl_interp_free(interpolation);
    }


    double first_derivative(const std::string &y_variable, double x) const override {
        
        const_cast<GSLDerivative*>(this)->set_y_variable(y_variable);

        if (xv.empty()) {
            throw std::runtime_error("x-variable must be set before calling first_derivative.");
        }
        
        return gsl_interp_eval_deriv(interpolation, xv.data(), yv.data(), x, acc);
        
    }


    double second_derivative(const std::string &y_variable, double x) const override {
        
        const_cast<GSLDerivative*>(this)->set_y_variable(y_variable);

        if (xv.empty()) {
            throw std::runtime_error("x-variable must be set before calling first_derivative.");
        }
        

        return gsl_interp_eval_deriv2(interpolation, xv.data(), yv.data(), x, acc);

    }

    double third_derivative(const std::string &y_variable, double x) const override {
        (void) y_variable;
        (void) x;
        return std::numeric_limits<double>::infinity();
    }


    void set_x_variable(const std::string &variable) override {
        if(saved_x_variable != variable){

            data.order_in(variable);
            xv = data.get_column(variable);
            saved_x_variable = variable;

            if(!yv.empty()){
                initialize_interpolator();
            }
        }
    }

    void set_y_variable(const std::string &variable) override{
        
        if(saved_y_variable != variable){
            yv = data.get_column(variable);
            saved_y_variable = variable;

            if(!xv.empty()){
                initialize_interpolator();
            }
        }
    }

};

class Derivatives {
private:
    std::unique_ptr<DerivativeStrategy> strategy;

public:
    Derivatives(std::unique_ptr<DerivativeStrategy> strat) : strategy(std::move(strat)) {}

    ~Derivatives(){}

    void set_strategy(std::unique_ptr<DerivativeStrategy> new_strategy) {
        strategy = std::move(new_strategy);
    }

    double get_first_derivative(std::string y_variable, double x) const {
        return strategy->first_derivative(y_variable, x);
    }

    double get_second_derivative(std::string y_variable, double x) const {
        return strategy->second_derivative(y_variable, x);
    }

    double get_third_derivative(std::string &y_variable, double x) const{
        return strategy->third_derivative(y_variable, x);
    }

    void set_x_variable(const std::string& variable) {
        strategy->set_x_variable(variable);
    }

    void set_y_variable(const std::string& variable) {
        strategy->set_y_variable(variable);
    }

};

}