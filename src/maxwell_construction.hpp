#pragma once

#ifndef MAXWELL_PHASE_TRANSITION_HPP
#define MAXWELL_PHASE_TRANSITION_HPP

#include <algorithm>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

#include "base_solver.hpp"
#include "constants.hpp"
#include "interpolator.hpp"
#include "log.hpp"

/*
    Forward declaration of EoS_Synthesis for the functors
*/
namespace Synthesis {

class MaxwellTransition;

}

/*
    Functor to compute the residuals of the Maxwell phase transition constraint for 1 dimensional EoS.

    This functor has the objective is to find the baryon chemical potential (mu_B_1 = mu_B_2) that satisfies
    the Maxwell constraint P_1 = P_2 for two EoS.

    The condition is checked for two given EoS including under fixed temperature.

    It uses the two sets of Synthesis::input_data and the interpolator to perform the necessary computations.
*/
struct Maxwell_phase_transition_functor {
   public:
    /**
     * @brief Constructor for the functor that computes the residuals of the Maxwell phase transition constraint for 1
     * dimensional EoS.
     *
     * @param MaxwellTransition & phase_transition_ Reference to the MaxwellTransition object
     */
    Maxwell_phase_transition_functor(Synthesis::DataSet<double>& M1_beta_, Synthesis::DataSet<double>& M2_beta_)
        : M1_beta(M1_beta_), M2_beta(M2_beta_) {}
    // ( Synthesis::MaxwellTransition & phase_transition_) : phase_transition(phase_transition_)
    // {}
    /**
     * @brief  Operator overload to compute the Maxwell constraint residuals.
     *      This method is utilized by an optimizer providing candidate variable values.
     * @param x Input array with independent variable values (muQ in this case)).
     * @param residuals Output array for storing computed residuals.
     * @return Boolean status of the operation.
     */
    template <typename T>
    bool operator()(const T* x, T* residuals) const;

   private:
    // Synthesis::MaxwellTransition & phase_transition;
    Synthesis::DataSet<double>& M1_beta;
    Synthesis::DataSet<double>& M2_beta;
};

namespace Synthesis {

/**
 * Class handling the computations related to the Maxwell phase transition
 */
class MaxwellTransition : public SynthesisMethod {
   private:
    double muB_critical;
    bool solution_found;

   public:
    MaxwellTransition(InputParams& input_params_) : SynthesisMethod(input_params_){};

    /**
     * @brief Computes a EoS based on two starting EoS using the Maxwell condition
     * @param M1_beta
     * @param M2_beta
     *  The eos are the only required datasets for this function.
     *            It must have the following columns:
     *            temperature, muB, muS, muQ, density, nS, nQ, energy_density, pressure, entropy_density
     *            More lines are allowed, but the first 10 columns must be in the order above.
     */
    void compute_synthesis() override{
        setup_solver_options();

        M1_beta.eos.read(input_params.files_M1_beta.eos);
        M2_beta.eos.read(input_params.files_M2_beta.eos);
        
        check_input_data();


        M1_beta.eos.setup_keys(keys_eos);
        M2_beta.eos.setup_keys(keys_eos);

        if (input_params.output_particle_properties) {
            M1_beta.particle_properties.read(input_params.files_M1_beta.particle_properties);
            M2_beta.particle_properties.read(input_params.files_M2_beta.particle_properties);
            
            if(M1_beta.particle_properties.get_data().empty() || M2_beta.particle_properties.get_data().empty()){
                input_params.output_particle_properties = false;
                InfoLog::getInstance().log(110, "Error with particle properties. Skipping output.");
            }else{
                M1_beta.particle_properties.setup_keys(keys_shared);
                M2_beta.particle_properties.setup_keys(keys_shared);
            }
        }

        // if (input_params.output_compOSE) {
        //     M1_beta.compOSE.read(input_params.files_M1_beta.compOSE);
        //     M2_beta.compOSE.read(input_params.files_M2_beta.compOSE);
            
        //     if(M1_beta.compOSE.get_data().empty() || M2_beta.compOSE.get_data().empty()){
        //         input_params.output_compOSE = false;
        //         InfoLog::getInstance().log(110, "Error with compOSE. Skipping output.");
        //     }else{
        //         M1_beta.compOSE.setup_keys(keys_shared);
        //         M2_beta.compOSE.setup_keys(keys_shared);
        //     }
        // }
        

        // if(input_params.output_flavor_equilibration){
        //     M1_beta.flavor_equilibration.read(input_params.files_M1_beta.flavor_equilibration);
        //     M2_beta.flavor_equilibration.read(input_params.files_M2_beta.flavor_equilibration);
        // }
       
        M1_beta.setup_data_set();
        M2_beta.setup_data_set();


        if(input_params.output_derivatives || input_params.check_eos_causality){
            try{
                M1_beta.derivatives= compute_derivatives(M1_beta.eos);
                M2_beta.derivatives= compute_derivatives(M2_beta.eos);
                M1_beta.derivatives.setup_keys(keys_derivatives);
                M2_beta.derivatives.setup_keys(keys_derivatives);
        
            }catch(const std::exception& e){
                InfoLog::getInstance().log(101, "Error in compute_derivatives: " + std::string(e.what()));
                throw std::runtime_error("Error in compute_derivatives: " + std::string(e.what()));
            }
        }

        M1_beta.order_in("muB");
        M2_beta.order_in("muB");
        output.setup_output_files();

        check_temperature_values();

        M1_beta.setup_interpolator_in("muB");
        M2_beta.setup_interpolator_in("muB");

        bool convergence;
        // todo: if(first_data_set.eos.empty())
        // output only second dataset

        convergence = compute_maxwell_transition(M1_beta.eos, M2_beta.eos);

        if (!convergence) {
            
            MultiLog::getInstance().log(101, "No solution found for Maxwell Phase Transition.");
            throw std::runtime_error("No solution found for Maxwell Phase Transition.");


        } else {
            InfoLog::getInstance().log(111, "Solution found for Maxwell Phase Transition. ");
            std::vector<double> M1_muB = M1_beta.eos.get_column("muB");
            std::vector<double> M2_muB = M2_beta.eos.get_column("muB");


            //calculate derivatives to get stable EoS            
            std::unique_ptr<DerivativeStrategy> strategy_1;
            std::unique_ptr<DerivativeStrategy> strategy_2;


            if (input_params.derivatives_method == "finite_difference") {
                strategy_1 = std::make_unique<FiniteDifferenceDerivative>(M1_beta.eos, input_params);
                strategy_2 = std::make_unique<FiniteDifferenceDerivative>(M2_beta.eos, input_params);
            } else if (input_params.derivatives_method == "gsl") {
                strategy_1 = std::make_unique<GSLDerivative>(M1_beta.eos, input_params);
                strategy_2 = std::make_unique<GSLDerivative>(M2_beta.eos, input_params);
            } else {
                throw std::invalid_argument("Unknown derivatives method specified in input_params.");
            }
            
            Derivatives derivatives_1(std::move(strategy_1));
            Derivatives derivatives_2(std::move(strategy_2));

            derivatives_1.set_x_variable("muB");
            derivatives_2.set_x_variable("muB");

            double dp_dmuB_1 = derivatives_1.get_first_derivative("pressure", muB_critical);
            double dp_dmuB_2 = derivatives_2.get_first_derivative("pressure", muB_critical);

            if (dp_dmuB_2 > dp_dmuB_1) {
                DataSet<double> M1_stable = remove_upper("muB", muB_critical, M1_beta);
                DataSet<double> M2_stable = remove_lower("muB", muB_critical, M2_beta);
                synthesized = join(M1_stable, M2_stable);
            } else {
                DataSet<double> M1_stable = remove_lower("muB", muB_critical, M1_beta);
                DataSet<double> M2_stable = remove_upper("muB", muB_critical, M2_beta);
                synthesized = join(M1_stable, M2_stable);
            }
      
            const size_t col_size= synthesized.eos.get_data()[0].size();
            if(col_size > keys_eos.size()){
                
                for(size_t i = keys_eos.size(); i < col_size; i++){
                    synthesized.eos.erase_column(i);
                }
            }

            synthesized.eos.setup_keys(keys_eos);

            std::vector<double> output_transition_vector = {M1_beta.eos.interpolate("temperature", muB_critical),
                                                            muB_critical,
                                                            M1_beta.eos.interpolate("muS", muB_critical),
                                                            M1_beta.eos.interpolate("muQ", muB_critical),
                                                            M1_beta.eos.interpolate("nB", muB_critical),
                                                            M1_beta.eos.interpolate("nS", muB_critical),
                                                            M1_beta.eos.interpolate("nQ", muB_critical),
                                                            M1_beta.eos.interpolate("energy_density", muB_critical),
                                                            M1_beta.eos.interpolate("pressure", muB_critical),
                                                            M1_beta.eos.interpolate("entropy_density", muB_critical)};

            // if (synthesized.eos.get_data()[0].size() > output_transition_vector.size()) {
            //     for (size_t i = output_transition_vector.size(); i < synthesized.eos.get_data()[0].size(); i++)
            //         output_transition_vector.push_back(M1_beta.eos.interpolate(i, muB_critical));
            // }

            


            auto it1 = std::find(M1_muB.begin(), M1_muB.end(), muB_critical);
            if (it1 == M1_muB.end()) synthesized.eos.push_back(output_transition_vector);

            output.transition.write(output_transition_vector);
            output.transition.skip_line();

            
            // use a depsilon in pressure so that qlimr can be used
            double dP=1e-8;
            output_transition_vector = {M2_beta.eos.interpolate("temperature", muB_critical),
                                        muB_critical,
                                        M2_beta.eos.interpolate("muS", muB_critical),
                                        M2_beta.eos.interpolate("muQ", muB_critical),
                                        M2_beta.eos.interpolate("nB", muB_critical),
                                        M2_beta.eos.interpolate("nS", muB_critical),
                                        M2_beta.eos.interpolate("nQ", muB_critical),
                                        M2_beta.eos.interpolate("energy_density", muB_critical),
                                        M2_beta.eos.interpolate("pressure", muB_critical) + dP,
                                        M2_beta.eos.interpolate("entropy_density", muB_critical)};



            auto it2 = std::find(M2_muB.begin(), M2_muB.end(), muB_critical);
            if (it2 == M2_muB.end()) synthesized.eos.push_back(output_transition_vector);
            

            
            
            if(input_params.output_derivatives){
                std::vector<double> output_derivatives_transition;

                output_derivatives_transition={M1_beta.eos.interpolate("temperature", muB_critical),
                                                                    muB_critical,
                                                                    M1_beta.eos.interpolate("muS", muB_critical),
                                                                    M1_beta.eos.interpolate("muQ", muB_critical),
                                                                    M1_beta.eos.interpolate("nB", muB_critical),
                                                                    M1_beta.eos.interpolate("nS", muB_critical),
                                                                    M1_beta.eos.interpolate("nQ", muB_critical),
                                                                    0.,
                                                                    INFINITY,
                                                                    INFINITY, 
                                                                    INFINITY
                };

                if (it1 == M1_muB.end()) synthesized.derivatives.push_back(output_derivatives_transition);

                output_derivatives_transition={M2_beta.eos.interpolate("temperature", muB_critical),
                                                                    muB_critical,
                                                                    M2_beta.eos.interpolate("muS", muB_critical),
                                                                    M2_beta.eos.interpolate("muQ", muB_critical),
                                                                    M2_beta.eos.interpolate("nB", muB_critical),
                                                                    M2_beta.eos.interpolate("nS", muB_critical),
                                                                    M2_beta.eos.interpolate("nQ", muB_critical),
                                                                    0.,
                                                                    INFINITY,
                                                                    INFINITY, 
                                                                    INFINITY
                };

                if (it2 == M2_muB.end()) synthesized.derivatives.push_back(output_derivatives_transition);
            }
            
            synthesized.eos.order_in("nB");

            for (auto row : synthesized.eos.get_data()) {
                output.eos.write(row);
                output.eos.skip_line();
            }


            if(input_params.output_derivatives){
                synthesized.derivatives.setup_keys(keys_derivatives);
                synthesized.derivatives.order_in("nB");

                for(auto row : synthesized.derivatives.get_data()){
                    output.derivatives.write(row);
                    output.derivatives.skip_line();
                }
            }

            if (input_params.output_particle_properties) {

                synthesized.particle_properties.setup_keys(keys_shared);
                
                std::vector<double> M1_critical= {
                            M1_beta.particle_properties.interpolate("temperature", muB_critical),
                            muB_critical,
                            0.,
                            0.,
                            M1_beta.particle_properties.interpolate("nB", muB_critical),
                            0.,
                            0.};

                for (size_t i = 7; i < synthesized.particle_properties.get_data()[0].size(); i++)
                    M1_critical.push_back(0.);


                auto it1 = std::find(M1_muB.begin(), M1_muB.end(), muB_critical);
                if (it1 == M1_muB.end()) synthesized.particle_properties.push_back(M1_critical);


                std::vector<double> M2_critical= {
                            M2_beta.particle_properties.interpolate("temperature", muB_critical),
                            muB_critical,
                            0.,
                            0.,
                            M2_beta.particle_properties.interpolate("nB", muB_critical),
                            0.,
                            0.};

                for (size_t i = 7; i < synthesized.particle_properties.get_data()[0].size(); i++)
                    M2_critical.push_back(0.);


                auto it2 = std::find(M2_muB.begin(), M2_muB.end(), muB_critical);
                if (it2 == M2_muB.end()) synthesized.particle_properties.push_back(M2_critical);

                synthesized.particle_properties.order_in("nB");

                for (auto row : synthesized.particle_properties.get_data()) {
                    output.particle_properties.write(row);
                    output.particle_properties.skip_line();
                }
            }

            // if(input_params.output_flavor_equilibration){
            //     for(auto row : synthesized.flavor_equilibration.get_data()){
            //         output.flavor_equilibration.write(row);
            //         output.flavor_equilibration.skip_line();
            //     }
            // }

        //     if (input_params.output_compOSE) {
        //         synthesized.compOSE.setup_keys(keys_shared);

        //         std::vector<double> M1_compOSE = {
        //                     M1_beta.compOSE.interpolate("temperature", muB_critical),
        //                     muB_critical,
        //                     M1_beta.compOSE.interpolate("muS", muB_critical),
        //                     M1_beta.compOSE.interpolate("muQ", muB_critical)};
                
        //         for (size_t i = 4; i < M1_beta.compOSE.get_data()[0].size(); i++)
        //             M1_compOSE.push_back(M1_beta.compOSE.interpolate(i, muB_critical));

        //         auto it1 = std::find(M1_muB.begin(), M1_muB.end(), muB_critical);
        //         if (it1 == M1_muB.end()) synthesized.compOSE.push_back(M1_compOSE);

        //         std::vector<double> M2_compOSE = {
        //                     M2_beta.compOSE.interpolate("temperature", muB_critical),
        //                     muB_critical,
        //                     M2_beta.compOSE.interpolate("muS", muB_critical),
        //                     M2_beta.compOSE.interpolate("muQ", muB_critical)};
                        
        //         for (size_t i = 4; i < M2_beta.compOSE.get_data()[0].size(); i++)
        //             M2_compOSE.push_back(M2_beta.compOSE.interpolate(i, muB_critical));
                
        //         auto it2 = std::find(M2_muB.begin(), M2_muB.end(), muB_critical);
        //         if (it2 == M2_muB.end()) synthesized.compOSE.push_back(M2_compOSE);

        //         synthesized.compOSE.order_in("nB");
                

        //         for (auto row : synthesized.compOSE.get_data()) {
        //             output.compOSE.write(row);
        //             output.compOSE.skip_line();
        //         }
        //     }


            if (input_params.verbose) std::cout << "\nFinished first order Maxwell phase transition.\n";

            
            eos_processing(synthesized);

            output.eos.close_file();
            output.particle_properties.close_file();
            // output.flavor_equilibration.close_file();
            // output.compOSE.close_file();
            output.transition.close_file();
            output.derivatives.close_file();
        }
    }

    /**
     * @brief Compute the Maxwell constraint for two EoS
     */
    template <typename T>
    bool compute_maxwell_transition(DataContainer<T> first_data_set, DataContainer<T> second_data_set) {
        std::vector<T> muB_vector_first = first_data_set.get_column("muB");
        std::vector<T> muB_vector_second = second_data_set.get_column("muB");

        double muB_min_overlap = calculate_minimum_overlap(muB_vector_first, muB_vector_second);
        double muB_max_overlap = calculate_maximum_overlap(muB_vector_first, muB_vector_second);

        // Set the initial guess for beta-equilibrium solver
        double muB_guess = (muB_min_overlap + muB_max_overlap) / 2.;
        double x[] = {muB_guess};

        // Define Ceres Solver variables:
        Problem problem;

        CostFunction* cost = new NumericDiffCostFunction<Maxwell_phase_transition_functor, ceres::CENTRAL, 1, 1>(
            new Maxwell_phase_transition_functor(M1_beta, M2_beta));

        problem.AddResidualBlock(cost, NULL, x);

        // Only look for values of muB values in the overlap region
        problem.SetParameterLowerBound(x, 0, muB_min_overlap);
        problem.SetParameterUpperBound(x, 0, muB_max_overlap);

        // Run
        Solve(options, &problem, &summary);

        print_solver_log();

        muB_critical = x[0];

        if (input_params.verbose) {
            std::cout << "muB_critical = " << muB_critical << std::endl;
        }

        return (summary.final_cost < input_params.solver.convergence_threshold && summary.IsSolutionUsable());
    }

    void eos_processing(DataSet<double> &data) override{

        // Stability check
        if (input_params.check_eos_stability) {
            check_stability(data);
            if (!data.is_stable()) {
                MultiLog::getInstance().log(400, "The interpolated EoS is not stable. Exiting.");
                throw std::runtime_error("The interpolated EoS is not stable. Exiting.");
            }
        }
        // Causality check
        if (input_params.check_eos_causality) {
            check_eos_causality(data);
            if (!data.is_causal()) {
                MultiLog::getInstance().log(400, "The interpolated EoS is not causal. Exiting.");
                throw std::runtime_error("The interpolated EoS is not causal. Exiting.");
            }
        }
    }


};  // end eos_synthesis class

}  // namespace Synthesis

template <typename T>
bool Maxwell_phase_transition_functor::operator()(const T* x, T* residuals) const {
    T muB_ = x[0];

    T pressure_1 = M1_beta.eos.interpolate("pressure", muB_);
    T pressure_2 = M2_beta.eos.interpolate("pressure", muB_);

    residuals[0] = pressure_1 - pressure_2;
    return true;
}


#endif