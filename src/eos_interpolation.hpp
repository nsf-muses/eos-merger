#pragma once

#ifndef INTERPOLATE_EOS_HPP
#define INTERPOLATE_EOS_HPP

#include <algorithm>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

#include "base_solver.hpp"
#include "constants.hpp"
#include "cubature.hpp"
#include "interpolator.hpp"
#include "log.hpp"

namespace Synthesis {



class EoSInterpolationStrategy{
public:

    SynthesisMethod & parent;
    DataContainer<double> synthesized;
    InputParams &input_params;
    double midpoint, Gamma;
    double lower_interpolation_limit, upper_interpolation_limit;


    EoSInterpolationStrategy(SynthesisMethod & parent_): parent(parent_),
    input_params(parent.input_params){

        midpoint = input_params.x_bar;
        Gamma = input_params.Gamma;

        lower_interpolation_limit= input_params.start_interpolation;
        upper_interpolation_limit= input_params.end_interpolation;

    };

    ~EoSInterpolationStrategy()=default;

    virtual void compute_interpolation() = 0;

    virtual void fill_synthesized_eos(DataContainer<double> &eos) {
     
        for (auto row : eos.get_data()) {
            synthesized.push_back(row);
        }
    }

    DataContainer<double> get_synthesized_eos() { return synthesized; }

    double f_minus(double x_, double x_bar_, double gamma_) { return (1. - tanh((x_ - x_bar_) / gamma_)) / 2.; }

    double f_plus(double x_, double x_bar_, double gamma_) { return (1. + tanh((x_ - x_bar_) / gamma_)) / 2.; }

    double g(double x_, double x_bar_, double gamma_) {
        return 2. / pow((exp(-(x_ - x_bar_) / gamma_) + exp((x_ - x_bar_) / gamma_)), 2.) / gamma_;
    }


    /**
     * @brief Integration wrapper for cubature.
     * @param dim: dimension of the integration
     * @param (func): function to be integrated
     * @param parametersPointer: pointer to the parameters of the function, in this case the particle
     * @param absolute_error: absolute error (0 to use only relative error)
     * @param relative_error: relative error
     * @param maxEval: maximum number of evaluations (0 sets no upper boundary on the maximum number of evaluations)
     * @return the integration of (func) over the limits set in the particle struct "limits"
    */

    double integrate(int dim, int(func)(unsigned ndim, const double *x, void *fdata, unsigned fdim, double *fval),
                     void *parametersPointer, double absolute_error = 0., double relative_error = 1e-5,
                     unsigned maxEval = 0) {
        double *val = (double *)malloc(sizeof(double));
        double *err = (double *)malloc(sizeof(double));
        double *xmin = (double *)malloc(dim * sizeof(double));
        double *xmax = (double *)malloc(dim * sizeof(double));

        xmin[0] = 0.;
        xmax[0] = 1.;

        hcubature(1, func, parametersPointer, dim, xmin, xmax, maxEval, absolute_error, relative_error,
                  ERROR_INDIVIDUAL, val, err);

        double result = val[0];
        free(val);
        free(err);
        free(xmin);
        free(xmax);
        return result;
    }




};

class P_of_muB_interpolation : public EoSInterpolationStrategy{

public:

    P_of_muB_interpolation(SynthesisMethod & parent_): EoSInterpolationStrategy(parent_){};

    ~P_of_muB_interpolation()=default;

    void compute_interpolation() override{


        if(input_params.verbose){
            std::cout << "\nInterpolating EoS with hyperbolic tangent as P(muB)." << std::endl;
            std::cout << "Pararameters: x_bar = " << midpoint << ", Gamma = " << Gamma << std::endl;
        }

        std::vector<double> M1_muB_vals = parent.M1_beta.eos.get_column("muB");
        std::vector<double> M2_muB_vals = parent.M2_beta.eos.get_column("muB");



        parent.M1_beta.eos.setup_interpolator_in("muB");
        parent.M2_beta.eos.setup_interpolator_in("muB");

        double muB_min_overlap = calculate_minimum_overlap(M1_muB_vals, M2_muB_vals);
        double muB_max_overlap = calculate_maximum_overlap(M1_muB_vals, M2_muB_vals);

        if(muB_min_overlap > midpoint || muB_max_overlap < midpoint){
           StatusLog::getInstance().setCode(406);
            throw std::runtime_error("The EoS do not overlap at the given baryon potential midpoint. Exiting.");
        }

        int iB = input_params.num_points;


        double muB_min = (muB_min_overlap < lower_interpolation_limit && !are_equal(input_params.start_interpolation, 0.)) 
                                                ? lower_interpolation_limit : muB_min_overlap;
        double muB_max = (muB_max_overlap > upper_interpolation_limit && !are_equal(input_params.end_interpolation, 0.)) 
                                                ? upper_interpolation_limit : muB_max_overlap;

        double dmuB = (muB_max - muB_min) / iB;

        double muB, pressure, energy, nB, entropy;
        double temperature=0.;


        DataSet<double> M1_reduced = parent.remove_upper("muB", muB_min, parent.M1_beta, true);
        DataSet<double> M2_reduced = parent.remove_lower("muB", muB_max, parent.M2_beta, true);
        
        if(!M1_reduced.eos.get_data().empty()){
            M1_reduced.eos.setup_keys(parent.keys_eos);
            M1_reduced.eos.order_in("nB");
            fill_synthesized_eos(M1_reduced.eos);
        }

        
        for (int i = 0; i <= iB; i++) {
            muB = muB_min + i * dmuB;

            double fm = f_minus(muB, midpoint, Gamma);
            double fp = f_plus(muB, midpoint, Gamma);

            pressure = fm * parent.M1_beta.eos.interpolate("pressure", muB) + fp * parent.M2_beta.eos.interpolate("pressure", muB);
            nB = fm * parent.M1_beta.eos.interpolate("nB", muB) + fp * parent.M2_beta.eos.interpolate("nB", muB);

            if (input_params.use_thermodynamic_consistency) {
                nB -= g(muB, midpoint, Gamma) *
                      (parent.M1_beta.eos.interpolate("pressure", muB) - parent.M2_beta.eos.interpolate("pressure", muB));
            }

            entropy = fm * parent.M1_beta.eos.interpolate("entropy_density", muB) +
                      fp * parent.M2_beta.eos.interpolate("entropy_density", muB);
            energy = -pressure + nB * muB + temperature * entropy;


            std::vector<double> row = {temperature, muB, 0., 0., nB, 0., 0., energy, pressure, entropy};

            synthesized.push_back(row);
           
        }

        if(!M2_reduced.eos.get_data().empty()){
            M2_reduced.eos.setup_keys(parent.keys_eos);
            M2_reduced.eos.order_in("nB");
            fill_synthesized_eos(M2_reduced.eos);
        }

    }

};


class E_of_nB_interpolation : public EoSInterpolationStrategy{

public:

    E_of_nB_interpolation(SynthesisMethod & parent_): EoSInterpolationStrategy(parent_){};
    ~E_of_nB_interpolation()=default;

    void compute_interpolation() override{


        if(input_params.verbose){
            std::cout << "\nInterpolating EoS with hyperbolic tangent as E(nB)." << std::endl;
            std::cout << "Pararameters: x_bar = " << midpoint << ", Gamma = " << Gamma << std::endl;
        }

        std::vector<double> M1_nB_vals = parent.M1_beta.eos.get_column("nB");
        std::vector<double> M2_nB_vals = parent.M2_beta.eos.get_column("nB");

        parent.M1_beta.setup_interpolator_in("nB");
        parent.M2_beta.setup_interpolator_in("nB");

        double nB_min_overlap = calculate_minimum_overlap(M1_nB_vals, M2_nB_vals);
        double nB_max_overlap = calculate_maximum_overlap(M1_nB_vals, M2_nB_vals);

        if(nB_min_overlap > midpoint || nB_max_overlap < midpoint){
            StatusLog::getInstance().setCode(406);
            throw std::runtime_error("The EoS do not overlap at the given baryon density midpoint. Exiting.");
        }

        int iB = input_params.num_points;

        double nB_min = (nB_min_overlap < lower_interpolation_limit && !are_equal(input_params.start_interpolation, 0.)) 
                                                ? lower_interpolation_limit : nB_min_overlap;
        double nB_max = (nB_max_overlap > upper_interpolation_limit && !are_equal(input_params.end_interpolation, 0.)) 
                                                ? upper_interpolation_limit : nB_max_overlap;

        double dnB = (nB_max - nB_min) / iB;

        double muB, pressure, energy, nB, entropy;
        double temperature=0.;



        DataSet<double> M1_reduced = parent.remove_upper("nB", nB_min, parent.M1_beta, true);
        DataSet<double> M2_reduced = parent.remove_lower("nB", nB_max, parent.M2_beta, true);


        fill_synthesized_eos(M1_reduced.eos);

        for (int i = 0; i <= iB; i++) {
            nB = nB_min + i * dnB;

            double fm = f_minus(nB, midpoint, Gamma);
            double fp = f_plus(nB, midpoint, Gamma);

            muB = fm * parent.M1_beta.eos.interpolate("muB", nB) + fp * parent.M2_beta.eos.interpolate("muB", nB);
            energy =
                fm * parent.M1_beta.eos.interpolate("energy_density", nB) + fp * parent.M2_beta.eos.interpolate("energy_density", nB);
            entropy = fm * parent.M1_beta.eos.interpolate("entropy_density", nB) +
                      fp * parent.M2_beta.eos.interpolate("entropy_density",
                                                   nB);  // add correction if fm and fp are functions of temperature

            if (input_params.use_thermodynamic_consistency) {
                muB -= g(nB, midpoint, Gamma) * (parent.M1_beta.eos.interpolate("energy_density", nB) -
                                               temperature * parent.M1_beta.eos.interpolate("entropy_density", nB) -
                                               parent.M2_beta.eos.interpolate("energy_density", nB) +
                                               temperature * parent.M2_beta.eos.interpolate("entropy_density", nB));
            }
            pressure = -energy + temperature * entropy + nB * muB;


            std::vector<double> row = {temperature, muB, 0., 0., nB, 0., 0., energy, pressure, entropy};

            synthesized.push_back(row);

        }

        fill_synthesized_eos(M2_reduced.eos);

    }

};



int energy_correction_for_P_of_nB(unsigned dim, const double *x, void *params_, unsigned fdim, double *retval);

struct integral_params {
    double lower_limit;
    double upper_limit;
    EoSInterpolationStrategy *synthesis;

    double x, midpoint, gamma;
};


class P_of_nB_interpolation : public EoSInterpolationStrategy{

public:

    P_of_nB_interpolation(SynthesisMethod & parent_): EoSInterpolationStrategy(parent_){};
    ~P_of_nB_interpolation()=default;

    void compute_interpolation() override{

        if(input_params.verbose){
            std::cout << "\nInterpolating EoS with hyperbolic tangent as P(nB)." << std::endl;
            std::cout << "Pararameters: x_bar = " << midpoint << ", Gamma = " << Gamma << std::endl;
        }

        integral_params parameters;
        parameters.synthesis = this;
        parameters.lower_limit = midpoint;
        parameters.midpoint = midpoint;
        parameters.gamma = Gamma;


        std::vector<double> M1_nB_vals = parent.M1_beta.eos.get_column("nB");
        std::vector<double> M2_nB_vals = parent.M2_beta.eos.get_column("nB");

        parent.M1_beta.setup_interpolator_in("nB");
        parent.M2_beta.setup_interpolator_in("nB");

        double nB_min_overlap = calculate_minimum_overlap(M1_nB_vals, M2_nB_vals);
        double nB_max_overlap = calculate_maximum_overlap(M1_nB_vals, M2_nB_vals);

        if(nB_min_overlap > midpoint || nB_max_overlap < midpoint){
            StatusLog::getInstance().setCode(406);
            throw std::runtime_error("The EoS do not overlap at the given baryon density midpoint. Exiting.");     
        }

        int iB = input_params.num_points;

        double nB_min = (nB_min_overlap < lower_interpolation_limit && !are_equal(input_params.start_interpolation, 0.)) 
                                                ? lower_interpolation_limit : nB_min_overlap;
        double nB_max = (nB_max_overlap > upper_interpolation_limit && !are_equal(input_params.end_interpolation, 0.)) 
                                                ? upper_interpolation_limit : nB_max_overlap;

        double dnB = (nB_max - nB_min) / iB;

        double muB, pressure, energy, nB, entropy;
        double temperature=0.;


        DataSet<double> M1_reduced = parent.remove_upper("nB", nB_min, parent.M1_beta, true);
        DataSet<double> M2_reduced = parent.remove_lower("nB", nB_max, parent.M2_beta, true);

        
        fill_synthesized_eos(M1_reduced.eos);

        for (int i = 0; i <= iB; i++) {
            nB = nB_min + i * dnB;

            double fm = f_minus(nB, midpoint, Gamma);
            double fp = f_plus(nB, midpoint, Gamma);

            pressure = fm * parent.M1_beta.eos.interpolate("pressure", nB) + fp * parent.M2_beta.eos.interpolate("pressure", nB);
            entropy = fm * parent.M1_beta.eos.interpolate("entropy_density", nB) +
                      fp * parent.M2_beta.eos.interpolate("entropy_density", nB);
            energy =
                fm * parent.M1_beta.eos.interpolate("energy_density", nB) + fp * parent.M2_beta.eos.interpolate("energy_density", nB);

            if (input_params.use_thermodynamic_consistency) {
                parameters.upper_limit = nB;
                energy += nB * integrate(1, &energy_correction_for_P_of_nB, &parameters) + temperature * entropy;
            }

            muB = (pressure + energy - temperature * entropy) / nB;

            std::vector<double> row = {temperature, muB, 0., 0., nB, 0., 0., energy, pressure, entropy};

            synthesized.push_back(row);

        }

        fill_synthesized_eos(M2_reduced.eos);

    }

};


class cs2_of_nB_interpolation : public EoSInterpolationStrategy{

public:
    
    cs2_of_nB_interpolation(SynthesisMethod & parent_): EoSInterpolationStrategy(parent_){};
    ~cs2_of_nB_interpolation()=default;
    
    void compute_interpolation() override{
            
        if(input_params.verbose){
            std::cout << "\nInterpolating EoS with hyperbolic tangent as cs2(nB)." << std::endl;
            std::cout << "Pararameters: x_bar = " << midpoint << ", Gamma = " << Gamma << std::endl;
        }

        int cs2_index=10;
        parent.M1_beta.eos.order_in("nB");
        parent.M2_beta.eos.order_in("nB");

        std::unique_ptr<DerivativeStrategy> strategy_1;
        std::unique_ptr<DerivativeStrategy> strategy_2;


        if (input_params.derivatives_method == "finite_difference") {
            strategy_1 = std::make_unique<FiniteDifferenceDerivative>(parent.M1_beta.eos, input_params);
            strategy_2 = std::make_unique<FiniteDifferenceDerivative>(parent.M2_beta.eos, input_params);
        } else if (input_params.derivatives_method == "gsl") {
            strategy_1 = std::make_unique<GSLDerivative>(parent.M1_beta.eos, input_params);
            strategy_2 = std::make_unique<GSLDerivative>(parent.M2_beta.eos, input_params);
        } else {
            throw std::invalid_argument("Unknown derivatives method specified in input_params.");
        }
        
        Derivatives derivatives_1(std::move(strategy_1));
        Derivatives derivatives_2(std::move(strategy_2));

        derivatives_1.set_x_variable("nB");
        derivatives_2.set_x_variable("nB");

        for(size_t i = 0; i < parent.M1_beta.eos.get_data().size(); i++) {
            double cs2 = derivatives_1.get_first_derivative("pressure", parent.M1_beta.eos.get_data()[i][baryon_density_index]) /
                         derivatives_1.get_first_derivative("energy_density", parent.M1_beta.eos.get_data()[i][baryon_density_index]);

            parent.M1_beta.eos.push_back(cs2, i);

        }

        for(size_t i = 0; i < parent.M2_beta.eos.get_data().size(); i++) {
            double cs2 = derivatives_2.get_first_derivative("pressure", parent.M2_beta.eos.get_data()[i][baryon_density_index]) /
                         derivatives_2.get_first_derivative("energy_density", parent.M2_beta.eos.get_data()[i][baryon_density_index]);

            parent.M2_beta.eos.push_back(cs2, i);

        }


        parent.M1_beta.eos.remove_nan_rows(cs2_index);
        parent.M2_beta.eos.remove_nan_rows(cs2_index);

        parent.M1_beta.eos.setup_interpolator_in("nB");
        parent.M2_beta.eos.setup_interpolator_in("nB");


        std::vector<double> M1_nB_vals = parent.M1_beta.eos.get_column("nB");
        std::vector<double> M2_nB_vals = parent.M2_beta.eos.get_column("nB");

        double nB_min_overlap = calculate_minimum_overlap(M1_nB_vals, M2_nB_vals);
        double nB_max_overlap = calculate_maximum_overlap(M1_nB_vals, M2_nB_vals);

        if(nB_min_overlap > midpoint || nB_max_overlap < midpoint){            
            StatusLog::getInstance().setCode(400);
            throw std::runtime_error("The EoS do not overlap at the given baryon density midpoint. Exiting.");
        }

        int iB = input_params.num_points;

        double nB_min = (nB_min_overlap < lower_interpolation_limit && !are_equal(input_params.start_interpolation, 0.))
                                        ? lower_interpolation_limit : nB_min_overlap;
        // double nB_max = (nB_max_overlap > upper_interpolation_limit && !are_equal(input_params.end_interpolation, 0.))
        //                                 ? upper_interpolation_limit : nB_max_overlap;


        
        double M1_nB_max= are_equal(input_params.end_interpolation, 0.) ? parent.M1_beta.eos.get_data().back()[baryon_density_index] : 
        std::max(parent.M1_beta.eos.get_data().back()[baryon_density_index], input_params.end_interpolation);
        double M2_nB_max= parent.M2_beta.eos.get_data().back()[baryon_density_index];

        double nB_max= M2_nB_max;
        double dnB = (nB_max - nB_min) / iB;
        double muB, nB, pressure, energy, entropy, cs2;
        double temperature=0.;

        DataSet<double> M1_reduced = parent.remove_upper("nB", nB_min, parent.M1_beta, true);
        DataSet<double> M2_reduced = parent.remove_lower("nB", nB_max, parent.M2_beta, true);

        
        fill_synthesized_eos(M1_reduced.eos);


        for (int i = 0; i <= iB; i++) {
            nB = nB_min + i * dnB;

            std::vector<double> previous_values = synthesized.get_data().back();

            double delta_nB = nB - previous_values[baryon_density_index]; // it can be different from dnB for the first point

            double fm = f_minus(nB, midpoint, Gamma);
            double fp = f_plus(nB, midpoint, Gamma);

            if(nB < M1_nB_max){
                cs2= fm * parent.M1_beta.eos.interpolate("cs2", nB) + fp * parent.M2_beta.eos.interpolate("cs2", nB);
            }else{
                cs2= parent.M2_beta.eos.interpolate("cs2", nB);
            }

            
            double muB_prev=  (previous_values[energy_density_index] + previous_values[pressure_index])/previous_values[baryon_density_index];
            energy= previous_values[energy_density_index] + delta_nB*muB_prev;
            pressure=previous_values[pressure_index]  + previous_values[cs2_index] *delta_nB * muB_prev;
            muB= (energy + pressure)/nB;
            entropy= 0.;
            

            std::vector<double> row = {temperature, muB, 0., 0., nB, 0., 0., energy, pressure, entropy, cs2};
            synthesized.push_back(row);

        }

        // fill_synthesized_eos(M2_reduced.eos);
        synthesized.erase_column(cs2_index);
    }

    //remove cs2 column from synthesized EoS:
};


class InterpolateEoS : public SynthesisMethod {
   private:
    std::vector<double> pressure_vals, muB_vals, nB_vals, energy_vals, entropy_vals, cs2_vals;

    double lower_interpolation_limit, upper_interpolation_limit;

   public:
    InterpolateEoS(InputParams &input_params_) : SynthesisMethod(input_params_){};



    double temperature = 0.;

    void compute_synthesis() override {
        setup_solver_options();

        M1_beta.eos.read(input_params.files_M1_beta.eos);
        M2_beta.eos.read(input_params.files_M2_beta.eos);

        check_input_data();

        std::vector<std::pair<size_t, std::string>> keys_beta = keys_eos;

        if(input_params.y_var_interp_method == "speed_of_sound_squared" 
                && input_params.x_var_interp_method == "baryon_density")
                keys_beta.push_back({10, "cs2"});


        M1_beta.eos.setup_keys(keys_beta);
        M2_beta.eos.setup_keys(keys_beta);


        if(input_params.output_particle_properties){
            input_params.output_particle_properties = false;
            InfoLog::getInstance().log(101, "Particle properties are not supported for the Interpolation method. Disabling output.");
        }

        output.setup_output_files();

        check_temperature_values();

        std::unique_ptr<EoSInterpolationStrategy> interpolation_strategy;
        if (input_params.y_var_interp_method == "pressure" &&
            input_params.x_var_interp_method == "baryon_chemical_potential") {
            interpolation_strategy = std::make_unique<P_of_muB_interpolation>(*this);
        } else if (input_params.y_var_interp_method == "pressure" &&
                input_params.x_var_interp_method == "baryon_density") {
            interpolation_strategy = std::make_unique<P_of_nB_interpolation>(*this);
        } else if (input_params.y_var_interp_method == "energy_density" &&
                input_params.x_var_interp_method == "baryon_density") {
            interpolation_strategy = std::make_unique<E_of_nB_interpolation>(*this);
        } else if (input_params.y_var_interp_method == "speed_of_sound_squared" &&
                input_params.x_var_interp_method == "baryon_density") {
            interpolation_strategy = std::make_unique<cs2_of_nB_interpolation>(*this);
        } else {
            std::cerr << "This set of Y(x) variables is not implemented in hyperbolic tangent interpolation. Exiting."
                    << std::endl;
            exit(1);
        }

        interpolation_strategy->compute_interpolation();

        synthesized.eos = interpolation_strategy->get_synthesized_eos();
    
        synthesized.eos.setup_keys(keys_eos);


        for (auto row : synthesized.eos.get_data()) {
            output.eos.write(row);
            output.eos.skip_line();
        }

        if (input_params.verbose)  std::cout << "\nFinished hyperbolic tangent interpolation.\n";


        eos_processing(synthesized);

        output.eos.close_file();
        output.derivatives.close_file();

    }

};

/**
 * @brief Function to be integrated to calculate the correction to the energy density in the P(nB) crossover
 * @param dim The dimension of the integral
 * @param x The integration variable
 * @param _particle The particle struct
 * @param fdim The dimension of the function to be integrated
 * @param retval The return value
 */
int energy_correction_for_P_of_nB(unsigned dim, const double *x, void *params_, unsigned fdim, double *retval) {
    (void)dim; /* avoid unused parameter warning */
    (void)fdim;

    integral_params &params = *reinterpret_cast<integral_params *>(params_);

    double nB;
    double jacobian = 1.;

    if (std::isinf(params.upper_limit)) {
        jacobian = 1. / (1. - x[0]) / (1. - x[0]);
        nB = x[0] / (1. - x[0]) + params.lower_limit;
    } else {
        jacobian = params.upper_limit - params.lower_limit;
        nB = params.lower_limit + x[0] * jacobian;
    }

    retval[0] = jacobian *
                (params.synthesis->parent.M1_beta.eos.interpolate("energy_density", nB) -
                 params.synthesis->parent.M2_beta.eos.interpolate("energy_density", nB)
                 ) *
                params.synthesis->g(nB, params.midpoint, params.gamma) / nB;
    return 0;
}

}  // namespace Synthesis

#endif