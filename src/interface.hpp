#pragma once

#ifndef SYNTHESIS_SOLVERS_HPP
#define SYNTHESIS_SOLVERS_HPP

#include "attach.hpp"
#include "eos_interpolation.hpp"
#include "gibbs_construction.hpp"
#include "input.hpp"
#include "maxwell_construction.hpp"
#include "log.hpp"

namespace Synthesis {

class EoS_Synthesis {
private:
    InputParams &input_params;
    std::unique_ptr<SynthesisMethod> synthesis_method;

public:

    EoS_Synthesis(InputParams &input_params_) : input_params(input_params_) {
        // Choose the appropriate synthesis method based on input_params
        if (input_params.synthesis_type == "attach") {
            synthesis_method = std::make_unique<AttachEoS>(input_params);
        } else if (input_params.synthesis_type == "maxwell") {
            synthesis_method = std::make_unique<MaxwellTransition>(input_params);
        } else if (input_params.synthesis_type == "gibbs") {
            synthesis_method = std::make_unique<GibbsTransition>(input_params);
        } else if (input_params.synthesis_type == "hyperbolic-tangent") {
            synthesis_method = std::make_unique<InterpolateEoS>(input_params);
        } else {
            throw std::invalid_argument("Unknown synthesis type specified in input_params.");
        }
    }

    void synthesize_eos() {
        std::cout << "\t\t\tRunning " << input_params.synthesis_type << " synthesis" << std::endl;
        
        try {
            synthesis_method->compute_synthesis();
        } catch (const std::exception &e) {
            StatusLog::getInstance().log(StatusLog::getInstance().getCode(), "Error executing " + input_params.synthesis_type + "." + std::string(e.what()));
            throw;
        }
    }

};  // end class

}  // namespace Synthesis

#endif
