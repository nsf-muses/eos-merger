#pragma once

#include <sys/stat.h>

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

#include "constants.hpp"
#include "log.hpp"
#include "utils.hpp"
#include "yaml-cpp/yaml.h"

using std::cout;
using std::endl;

namespace Synthesis {

/**
 * Struct to store the input file names from YAML file
 */
struct InputFiles {
    std::string eos;
    std::string particle_properties;
    // std::string flavor_equilibration;
    // std::string compOSE;
};


struct SolverParams{
    double parameter_tolerance;
    double function_tolerance;
    double gradient_tolerance;
    bool use_nonmonotonic_steps;
    int max_iterations;
    std::string method;
    std::string linear_solver;
    double convergence_threshold;
};

/**
 * Struct to store the input parameters from YAML file
 */
struct InputParams {
    // General
    std::string run_name;
    std::string output_path;
    std::string synthesis_type;
    int verbose;
    bool check_eos_stability;
    bool check_eos_causality;

    InputFiles files_M1_beta;
    InputFiles files_M2_beta;
    InputFiles files_M1_grid;
    InputFiles files_M2_grid;
    InputFiles files_lepton;

    bool output_particle_properties;
    bool output_compOSE;
    // bool output_flavor_equilibration;
    bool output_derivatives;

    // Attach method
    std::string atch_key;
    double atch_value;

    // EoS interpolation method
    int num_points;
    std::string y_var_interp_method, x_var_interp_method;
    bool use_thermodynamic_consistency;
    double Gamma;
    double x_bar;
    double start_interpolation;
    double end_interpolation;

    // Derivatives
    double step_size;
    std::string step_type;
    int precision;
    std::string derivatives_method;

    SolverParams solver;
};

// Class to read the input parameters from YAML file
class Input {
   private:
    const std::string config_path;  // Path to the configuration YAML file
    YAML::Node config_node;         // Holds the YAML configuration data
   public:
    InputParams input_params;  // Struct to store the input parameters

    Input(){};


    Input(std::string &config_path_) : config_path(config_path_) { create_input_params(); }


    void create_input_params() {
        config_node = read_YAML_file();

        const std::string key_global = "global";
        const std::string key_attach = "attach_method";
        const std::string key_interpolation = "interpolation_method";
        const std::string key_derivatives = "derivatives";
        const std::string key_output = "output";
        const std::string key_mixed_phase = "mixed_phase";
        const std::string key_solver = "solver";

        try {
            input_params.run_name = config_node[key_global]["run_name"].as<std::string>();
            input_params.verbose = config_node[key_global]["verbose"].as<int>();

            input_params.synthesis_type = config_node[key_global]["synthesis_type"].as<std::string>();
            input_params.check_eos_stability = config_node[key_global]["check_eos_stability"].as<bool>();  
            input_params.check_eos_causality = config_node[key_global]["check_eos_causality"].as<bool>();


            input_params.files_M1_beta = set_eos_files("model1_BetaEq_files");
            input_params.files_M2_beta = set_eos_files("model2_BetaEq_files");
            input_params.files_lepton = set_eos_files("lepton_files");
            input_params.files_M1_grid = set_eos_files("model1_Grid_files");
            input_params.files_M2_grid = set_eos_files("model2_Grid_files");

            input_params.output_particle_properties = config_node[key_output]["output_particle_properties"].as<bool>();
            input_params.output_compOSE = config_node[key_output]["output_compOSE"].as<bool>();
            input_params.output_derivatives = config_node[key_output]["output_derivatives"].as<bool>();


            if(input_params.synthesis_type == "gibbs"){
                input_params.num_points= config_node[key_mixed_phase]["number_of_points"].as<int>();
            }

            if (input_params.synthesis_type == "attach") {  // Attach method (type 0
                input_params.atch_key = config_node[key_attach]["attach_variable"].as<std::string>();
                input_params.atch_value = config_node[key_attach]["attach_value"].as<double>();
            }
            
            if (input_params.synthesis_type == "hyperbolic-tangent") {  // Interpolation method (type 1
                input_params.num_points = config_node[key_interpolation]["number_of_points"].as<int>();
                input_params.y_var_interp_method = config_node[key_interpolation]["y_variable"].as<std::string>();
                input_params.x_var_interp_method = config_node[key_interpolation]["x_variable"].as<std::string>();
                input_params.Gamma = config_node[key_interpolation]["Gamma"].as<double>();
                input_params.x_bar = config_node[key_interpolation]["x_variable_midpoint"].as<double>();
                input_params.use_thermodynamic_consistency =
                    config_node[key_interpolation]["use_thermodynamic_consistency"].as<bool>();
            
                input_params.start_interpolation = config_node[key_interpolation]["start_interpolation"].as<double>();
                input_params.end_interpolation = config_node[key_interpolation]["end_interpolation"].as<double>();
                

                if (input_params.y_var_interp_method == "energy_density" &&
                    input_params.x_var_interp_method == "chemical_potential") {
                    std::string message = "Error: The interpolation method is not valid for the given variables";
                    std::cout << message << std::endl;
                    MultiLog::getInstance().log(400, message);
                }
            }

            
            input_params.derivatives_method = config_node[key_derivatives]["method"].as<std::string>();
            if(input_params.derivatives_method == "finite_difference"){
                input_params.step_size= config_node[key_derivatives][input_params.derivatives_method]["step_size"].as<double>();
                input_params.step_type= config_node[key_derivatives][input_params.derivatives_method]["step_type"].as<std::string>();
                input_params.precision = config_node[key_derivatives][input_params.derivatives_method]["precision"].as<int>();
            }

            
            input_params.solver.parameter_tolerance = config_node[key_solver]["parameter_tolerance"].as<double>();
            input_params.solver.function_tolerance  = config_node[key_solver]["function_tolerance"].as<double>();
            input_params.solver.gradient_tolerance  = config_node[key_solver]["gradient_tolerance"].as<double>();
            input_params.solver.use_nonmonotonic_steps = config_node[key_solver]["use_nonmonotonic_steps"].as<bool>();
            input_params.solver.max_iterations = config_node[key_solver]["max_num_iterations"].as<int>();
            input_params.solver.method = config_node[key_solver]["method"].as<std::string>();
            input_params.solver.linear_solver = config_node[key_solver]["linear_solver"].as<std::string>();
            input_params.solver.convergence_threshold = config_node[key_solver]["convergence_threshold"].as<double>();
            

        } catch (const std::exception &e) {
            MultiLog::getInstance().log(400, "Error input config parameters. " + std::string(e.what()));
            std::throw_with_nested(std::runtime_error("Error input config parameters"));
        }

        if (!input_params.run_name.empty()) {
            try {
                input_params.output_path = OUTPUT_PATH + input_params.run_name + "/";
                std::filesystem::create_directories(input_params.output_path);
                InfoLog::getInstance().move(input_params.output_path);
            } catch (const std::filesystem::filesystem_error &e) {
                if (e.code() == std::errc::permission_denied) {
                    const std::string err_msg = "Permission denied for creating output directory. " + input_params.output_path;
                    StatusLog::getInstance().log(403, err_msg);
                    throw std::runtime_error(err_msg);
                }
                throw;
            }
        } else {
            input_params.output_path = OUTPUT_PATH;
        }
    }


    YAML::Node read_YAML_file() const {
        if (!Synthesis::file_exists(config_path)) {
            const std::string err_msg = "Inexistent YAML file. " + config_path;
            StatusLog::getInstance().log(404, err_msg);
            throw std::runtime_error(err_msg);
        }
            
        try {
            return YAML::LoadFile(config_path);
        } catch (const std::exception &e) {
            const std::string err_msg = "Problems reading YAML file. " + std::string(e.what());
            StatusLog::getInstance().log(400, err_msg);
            throw std::runtime_error(err_msg);
        }
    }


    void print_YAML_node() const {
        for (YAML::const_iterator it = config_node.begin(); it != config_node.end(); ++it) {
            std::cout << it->first.as<std::string>() << "\t" << config_node[it->first.as<std::string>()] << std::endl;
        }
    }


    InputFiles set_eos_files(const std::string &key) {
        InputFiles file_name;
        file_name.eos = INPUT_PATH + config_node[key]["eos_input_file"].as<std::string>();
        file_name.particle_properties =
            INPUT_PATH + config_node[key]["particle_properties_input_file"].as<std::string>();
        return file_name;
    }
};

}  // namespace Synthesis
