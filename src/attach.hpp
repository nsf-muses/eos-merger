#pragma once

#ifndef ATTACH_HPP
#define ATTACH_HPP

#include <algorithm>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

#include "base_solver.hpp"
#include "constants.hpp"
#include "log.hpp"

namespace Synthesis {

/**
 * @brief Attaches two EoS
 */
class AttachEoS : public SynthesisMethod {
   public:
    AttachEoS(InputParams &input_params_) : SynthesisMethod(input_params_){};

    /**
     * @brief Computes a EoS based on two starting EoS using the Maxwell condition
     * @param M1_beta
     * @param M2_beta
     *  The eos are the only required datasets for this function.
     *            It must have the following columns:
     *            temperature, muB, muS, muQ, density, nS, nQ, energy_density, pressure, entropy_density
     *            More lines are allowed, but the first 10 columns must be in the order above.
     */
    void compute_synthesis() override {
        M1_beta.eos.read(input_params.files_M1_beta.eos);
        M2_beta.eos.read(input_params.files_M2_beta.eos);
        
        check_input_data();
        
        M1_beta.eos.setup_keys(keys_eos);
        M2_beta.eos.setup_keys(keys_eos);

        if (input_params.output_particle_properties) {
            M1_beta.particle_properties.read(input_params.files_M1_beta.particle_properties);
            M2_beta.particle_properties.read(input_params.files_M2_beta.particle_properties);
            M1_beta.particle_properties.setup_keys(keys_eos);
            M2_beta.particle_properties.setup_keys(keys_eos);
        }
        
        input_params.atch_key = normalize_input_key(input_params.atch_key);


        M1_beta.order_in(input_params.atch_key);
        M2_beta.order_in(input_params.atch_key);
        output.setup_output_files();

        check_temperature_values();

        if(input_params.verbose)
            std::cout << "\nAttaching the EoS' at " << input_params.atch_key << " = " << input_params.atch_value << std::endl;

        DataSet<double> M1_stable = remove_upper(input_params.atch_key, input_params.atch_value, M1_beta);
        DataSet<double> M2_stable = remove_lower(input_params.atch_key, input_params.atch_value, M2_beta);

        synthesized = join(M1_stable, M2_stable);
        synthesized.eos.setup_keys(keys_eos);

        synthesized.eos.order_in("nB");

        const size_t col_size= synthesized.eos.get_data()[0].size();
        if(col_size > keys_eos.size()){
            for(size_t i = keys_eos.size(); i < col_size; i++){
                        synthesized.eos.erase_column(i);
            }
        }

        for (auto row : synthesized.eos.get_data()) {
            output.eos.write(row);
            output.eos.skip_line();
        }

        if (input_params.output_particle_properties) {
            for (auto row : synthesized.particle_properties.get_data()) {
                output.particle_properties.write(row);
                output.particle_properties.skip_line();
            }
        }

        if (input_params.verbose) std::cout << "\nFinished Attaching the EoS'.\n";

        eos_processing(synthesized);

        output.eos.close_file();
        output.particle_properties.close_file();
        output.transition.close_file();
        output.derivatives.close_file();

    }

};  // end Synthesis class

}  // namespace Synthesis

#endif