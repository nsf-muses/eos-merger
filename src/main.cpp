#include <sys/stat.h>

#include "interface.hpp"

// using namespace eos_synthesis;

int main(int argc, char *argv[]) {
    std::string config_yaml_input = Synthesis::INPUT_PATH + "config.yaml";

    if (argc == 2) {                     // One argument given. Use argv[1]_config.yaml
        std::string yaml_str = argv[1];  // Get the argument as a string
        config_yaml_input = "../input/" + yaml_str + "/config.yaml";
    }

    cout << "\n Using config.yaml file in the directory " + config_yaml_input + "\n";

    Synthesis::StatusLog::getInstance().setOutputPath(Synthesis::OUTPUT_PATH);
    Synthesis::InfoLog::getInstance().setOutputPath(Synthesis::OUTPUT_PATH);
    

    // Create object for class of input parameters
    Synthesis::Input input(config_yaml_input);

    // Create eos_synthesis_module object
    Synthesis::EoS_Synthesis eos_synthesis(input.input_params);

    eos_synthesis.synthesize_eos();

    // Copy input YAML config to the output folder
    Synthesis::copy_file(config_yaml_input, input.input_params.output_path, "config.yaml");

    Synthesis::MultiLog::getInstance().log(200, "Successful Synthesis run!");

    if(!input.input_params.run_name.empty())
        Synthesis::StatusLog::getInstance().copy(input.input_params.output_path);
    

    return 0;
}