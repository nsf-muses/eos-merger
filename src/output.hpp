#pragma once

#include <cstdio>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <mutex>

#include "input.hpp"
#include "log.hpp"

namespace Synthesis {

// Class dedicated to output operations
class OutputHandler {
   private:
    std::ofstream output;
    std::mutex mtx;
    std::string filename;

   public:
    OutputHandler() {}

    void open_file(const std::string &filename_) {
        std::lock_guard<std::mutex> lock(mtx);
        filename = filename_;
        output.open(filename);

        output << std::uppercase << std::scientific << std::setprecision(OUTPUT_PRECISION);

        if (!output.is_open()) {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Error opening output file " + filename);
        }
    }

    template <typename T>
    void write(const T &data, bool include_separator = true) {
        std::lock_guard<std::mutex> lock(mtx);
        if (output.is_open()) {
            output << data;
            if (include_separator) {
                output << OUTPUT_SEPARATOR;
            }
        } else {
            MultiLog::getInstance().log(500, "Error. File " + filename + " is not open for writing!");
        }
    }

    inline bool is_open() const { return output.is_open(); }

    template <typename T>
    void write(const std::vector<T> &data) {
        std::lock_guard<std::mutex> lock(mtx);
        if (output.is_open()) {
            if (!data.empty()) {
                for (size_t i = 0; i < data.size(); ++i) {
                    output << data[i];

                    // Add the separator only if this is not the last element
                    if (i < data.size() - 1) output << OUTPUT_SEPARATOR;
                }
            }
        } else {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Error. File " + filename + " is not open for writing!");
        }
    }

    void skip_line() {
        std::lock_guard<std::mutex> lock(mtx);
        if (output.is_open()) {
            output << std::endl;
        } else {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Error. File " + filename + " is not open for writing!");
        }
    }

    void close_file() {
        std::lock_guard<std::mutex> lock(mtx);
        if (output.is_open()) {
            output.close();
        }
    }

};  // end output class

class Output {
   private:
    InputParams &input_params;

   public:
    Output(InputParams &input_) : input_params(input_) {}

    OutputHandler eos;
    OutputHandler transition;
    OutputHandler particle_properties;
    OutputHandler derivatives;

    void setup_output_files() {
        std::string output_path =
            input_params.run_name.empty() ? OUTPUT_PATH : OUTPUT_PATH + input_params.run_name + "/";

        try{
            eos.open_file(output_path + "eos.csv");
        } catch (const std::exception &e) {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Error opening EoS output file. " + std::string(e.what()));
        }

        try{
            transition.open_file(output_path + "transition.csv");
        } catch (const std::exception &e) {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Error opening transition output file. " + std::string(e.what()));
        }

        if (input_params.output_particle_properties){
            try{
                particle_properties.open_file(output_path + "particle_properties.csv");

            } catch (const std::exception &e) {
                StatusLog::getInstance().setCode(500);
                throw std::runtime_error("Error opening particle properties output file. " + std::string(e.what()));
            }
        }
        if (input_params.output_derivatives){
            try{
                derivatives.open_file(output_path + "eos_derivatives.csv");
            }catch (const std::exception &e) {
                StatusLog::getInstance().setCode(500);
                throw std::runtime_error("Error opening derivatives output file. " + std::string(e.what()));
            }
        }
    }
};

}  // namespace Synthesis