#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <mutex>
#include <stdexcept>
#include <string>

#include "constants.hpp"

namespace Synthesis {
    
class Log {
   protected:
    std::filesystem::path file_path{};
    std::string file_name{};
    std::ofstream log_file;
    std::mutex log_mtx;
    int code = 500;

    Log() = default;

   public:
    Log(const Log&) = delete;
    Log& operator=(const Log&) = delete;

    virtual ~Log() {
        if (log_file.is_open()) {
            log_file.close();
        }
    }

    static Log& getInstance() {
        static Log instance;
        return instance;
    }

    void log(int log_value, const std::string& message) {
        std::lock_guard<std::mutex> guard(log_mtx);
        if (!log_file.is_open()) {
            throw std::runtime_error("Error writing to log: Log file is not open.");
        }
        log_file << "code: " << log_value << "\n"
                 << "message: " << message << "\n";        
        log_file.flush();

    }

    void setCode(int code) {
        this->code = code;
    }

    int getCode() {
        return code;
    }

    void setOutputPath(const std::filesystem::path& output_path) {
        file_path = output_path;
        log_file.open((file_path / file_name).string(), std::ios::out);
        if (!log_file.is_open()) {
            throw std::runtime_error("Unable to open file: " + (file_path / file_name).string());
        }
    }

    void move(const std::string& new_log_path) {
        std::lock_guard<std::mutex> guard(log_mtx);
        if (log_file.is_open()) {
            log_file.close();
        }

        std::filesystem::path old_path = file_path;
        std::filesystem::path new_path = new_log_path;
        std::filesystem::rename( (old_path / file_name).string() , (new_path / file_name).string() );
        file_path = new_log_path;
        log_file.open( (file_path / file_name).string(), std::ios::out | std::ios::app);
    }

    void copy(const std::string& new_log_path) {
        std::lock_guard<std::mutex> guard(log_mtx);

    if (log_file.is_open()) {
        log_file.flush();
        log_file.close();
    }
        std::filesystem::path old_path = file_path;
        std::filesystem::path new_path = new_log_path;
        std::filesystem::copy( (old_path / file_name).string() , (new_path / file_name).string() , std::filesystem::copy_options::overwrite_existing);
       
    }

};

class InfoLog : public Log {
   private:
    InfoLog() {
        file_name = "info.yaml";
    }

   public:
    static InfoLog& getInstance() {
        static InfoLog instance;
        return instance;
    }

};

class StatusLog : public Log {
   private:
    StatusLog() {
        file_name = "status.yaml";
    }

   public:
    static StatusLog& getInstance() {
        static StatusLog instance;
        return instance;
    }

};


class MultiLog {
private:
    std::vector<Log*> logs;

    MultiLog() {
        logs.push_back(&InfoLog::getInstance());
        logs.push_back(&StatusLog::getInstance());
    }

public:
    static MultiLog& getInstance() {
        static MultiLog instance;
        return instance;
    }

    void log(int logValue, const std::string& message) {
        for (auto log : logs) {
            log->log(logValue, message);
        }
    }
};

}  // namespace Synthesis