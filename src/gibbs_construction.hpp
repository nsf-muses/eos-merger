#pragma once

#ifndef GIBBS_PHASE_TRANSITION_HPP
#define GIBBS_PHASE_TRANSITION_HPP

#include <algorithm>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

#include "base_solver.hpp"
#include "constants.hpp"
#include "log.hpp"

/*
    Functor to compute the residuals of the Maxwell phase transition constraint for 1 dimensional EoS.

    This functor has the objective is to find the baryon chemical potential (mu_B_1 = mu_B_2) that satisfies
    the Maxwell constraint P_1 = P_2 for two EoS.

    The condition is checked for two given EoS including under fixed temperature.

    It uses the two sets of Synthesis::input_data and the interpolator to perform the necessary computations.
*/
class Gibbs_phase_transition_functor {
   private:
    double muB, muQ;

   public:
    /**
     * @brief Constructor for the functor that computes the residuals of the Maxwell phase transition constraint for 1
     * dimensional EoS.
     *
     * @param GibbsTransition object with the data and methods to compute the Gibbs constraint
     * @param first_data_set_ Synthesis::input_data object with the first EoS data
     * @param second_data_set_ Synthesis::input_data object with the second EoS data
     * @param lepton_data_set_ Synthesis::input_data object with the lepton EoS data
     * @param chi_ value of the vol_M1 parameter for the Gibbs construction
     */
    Gibbs_phase_transition_functor(double vol_M1_, Synthesis::DataSet<double>& M1_, Synthesis::DataSet<double>& M2_,
                                   Synthesis::DataSet<double>& leptons_)
        : M1(M1_), M2(M2_), leptons(leptons_) {
        vol_M1 = vol_M1_;
    }
    /**
     * @brief  Operator overload to compute the Maxwell constraint residuals.
     *      This method is utilized by an optimizer providing candidate variable values.
     * @param x Input array with independent variable values (muQ in this case)).
     * @param residuals Output array for storing computed residuals.
     * @return Boolean status of the operation.
     */
    template <typename T>
    bool operator()(const T* x, T* residuals) const;

   private:
    double vol_M1;
    Synthesis::DataSet<double>& M1;
    Synthesis::DataSet<double>& M2;
    Synthesis::DataSet<double>& leptons;
    // Synthesis::Mixed_phase & gibbs;
};

namespace Synthesis {

/**
 * Class handling the computations related to the Maxwell phase transition
 */
class GibbsTransition : public SynthesisMethod {
   public:
    double vol_M1;  /// Volume fraction of the first phase in the mixed phase.
                    // vol_M2 = 1-vol_M1
    double muB, muQ;

    GibbsTransition(InputParams& input_params_) : SynthesisMethod(input_params_){};
    /**
     * @brief Computes a EoS based on two starting EoS using the Maxwell condition
     * @param files_M1_grid
     * @param files_M2_grid
     *  The eos are the only required datasets for this function.
     *            It must have the following columns:
     *            temperature, muB, muQ, density, nS, nQ, energy_density, pressure, entropy_density
     *            More lines are allowed, but the first 10 columns must be in the order above.
     */
    void compute_synthesis() override {
        setup_solver_options();

        M1_grid.eos.read(input_params.files_M1_grid.eos);
        M2_grid.eos.read(input_params.files_M2_grid.eos);
        leptons.eos.read(input_params.files_lepton.eos);

        M1_grid.eos.setup_keys(keys_eos);
        M2_grid.eos.setup_keys(keys_eos);
        leptons.eos.setup_keys(keys_leptons);

        synthesized.eos.setup_keys(keys_eos);

        M1_grid.eos.order_in("temperature", "muB", "muQ");
        M2_grid.eos.order_in("temperature", "muB", "muQ");
        leptons.eos.order_in("temperature", "muE", "muNu");

        if (input_params.output_particle_properties) {
            M1_grid.particle_properties.read(input_params.files_M1_grid.particle_properties);
            M2_grid.particle_properties.read(input_params.files_M2_grid.particle_properties);
            leptons.particle_properties.read(input_params.files_lepton.particle_properties);

            synthesized.particle_properties.setup_keys(keys_shared);
            if(M1_grid.particle_properties.get_data().size() == 0 || M2_grid.particle_properties.get_data().size() == 0 || leptons.particle_properties.get_data().size() == 0){
                InfoLog::getInstance().log(151, "Error with Particle properties input files. Skipping output.");
                input_params.output_particle_properties = false;
            }else{
                M1_grid.particle_properties.setup_keys(keys_shared);
                M2_grid.particle_properties.setup_keys(keys_shared);
                leptons.particle_properties.setup_keys(keys_leptons);
            }
        }

        // if (input_params.output_compOSE) {

        //     M1_grid.compOSE.read(input_params.files_M1_grid.compOSE);
        //     M2_grid.compOSE.read(input_params.files_M2_grid.compOSE);
        //     leptons.compOSE.read(input_params.files_lepton.compOSE);

        //     synthesized.compOSE.setup_keys(keys_shared);

        //     if(M1_grid.compOSE.get_data().size() == 0 || M2_grid.compOSE.get_data().size() == 0 || leptons.compOSE.get_data().size() == 0){
        //         InfoLog::getInstance().log(151, "Error with compOSE input files. Skipping output.");
        //         input_params.output_compOSE = false;
        //     }else{
        //         M1_grid.compOSE.setup_keys(keys_shared);
        //         M2_grid.compOSE.setup_keys(keys_shared);
        //         leptons.compOSE.setup_keys(keys_leptons);
        //     }
        // }

        M1_grid.setup_data_set();
        M2_grid.setup_data_set();
        leptons.setup_data_set();

        // if(input_params.output_flavor_equilibration){
        //     M1_grid.flavor_equilibration.read(input_params.files_M1_grid.flavor_equilibration);
        //     M2_grid.flavor_equilibration.read(input_params.files_M2_grid.flavor_equilibration);
        //     leptons.flavor_equilibration.read(input_params.files_lepton.flavor_equilibration);
        // }

        M1_grid.order_in("temperature", "muB", "muQ");
        M2_grid.order_in("temperature", "muB", "muQ");
        leptons.order_in("temperature", "muE");

        output.setup_output_files();

        // Erase rows in nuclear grid with muS!=0 to do beta-equilibrated mixed phase
        M1_grid.erase_rows_with_diff_vals("muS", 0.);
        M2_grid.erase_rows_with_diff_vals("muS", 0.);
        leptons.erase_rows_with_diff_vals("muNu", 0.);


        if (input_params.verbose) {
            std::cout << "Lines in the first eos file: " << M1_grid.eos.get_data().size() << std::endl;
            std::cout << "Lines in the second eos file: " << M2_grid.eos.get_data().size() << std::endl;
            std::cout << "Lines in the lepton eos file: " << leptons.eos.get_data().size() << std::endl;
        }

        M1_grid.setup_interpolator_in("muB", "muQ");
        M2_grid.setup_interpolator_in("muB", "muQ");
        leptons.setup_interpolator_in("muE");
        
        size_t i_temp_in_files = std::max(M1_grid.eos.get_unique_values_of("temperature").size(),
                                          M2_grid.eos.get_unique_values_of("temperature").size());

        i_temp_in_files = std::max(i_temp_in_files, leptons.eos.get_unique_values_of("temperature").size());
        // Temporary:
        if (i_temp_in_files > 1) {
            StatusLog::getInstance().setCode(400);
            throw std::runtime_error("Currently the Synthesis Module only accepts files with a single temperature value. Exiting.");
        }

        double temperature = 0.;

        // setup initial conditions
        std::vector<double> muB_vector_first = M1_grid.eos.get_column("muB");
        std::vector<double> muB_vector_second = M2_grid.eos.get_column("muB");
        double muB_min_overlap = calculate_minimum_overlap(muB_vector_first, muB_vector_second);
        double muB_max_overlap = calculate_maximum_overlap(muB_vector_first, muB_vector_second);

        std::vector<double> muQ_vector_first = M1_grid.eos.get_column("muQ");
        std::vector<double> muQ_vector_second = M2_grid.eos.get_column("muQ");
        double muQ_min_overlap = calculate_minimum_overlap(muQ_vector_first, muQ_vector_second);
        double muQ_max_overlap = calculate_maximum_overlap(muQ_vector_first, muQ_vector_second);

        M1_grid.setup_interpolator_in("muB", "muQ");
        M2_grid.setup_interpolator_in("muB", "muQ");
        leptons.setup_interpolator_in("muE");
        // Set the initial guess the solver

        muB = (muB_min_overlap + muB_max_overlap) / 2.;
        muQ = (muQ_min_overlap + muQ_max_overlap) / 2.;

        vol_M1 = 0.;
        int i_vol_total = input_params.num_points;
        
        double vol_M1_step = 1. / i_vol_total;

        for (int ivol = 0; ivol < i_vol_total; ivol++) {
            vol_M1 = ivol * vol_M1_step;

            bool convergence =
                compute_gibbs_phase_transition(muB_min_overlap, muB_max_overlap, muQ_min_overlap, muQ_max_overlap);


            if (!convergence) {
                if (input_params.verbose > 1) {
                    std::cout << std::setw(30) << std::left << " --- Solution is not usable / no convergence"
                              << std::endl;

                    InfoLog::getInstance().log(
                        151, "Cannot find mixed phase solution for vol fraction of =" + std::to_string(vol_M1));
                }

            } else {
                if (input_params.verbose > 1)
                    std::cout << std::setw(30) << std::left << " --- Solution found" << std::endl;

                std::vector<double> muE_lepton = leptons.eos.get_column("muE");

                ///> Write EoS output to file

                double muS_ = (1. - vol_M1) * M1_grid.eos.interpolate("muS", muB, muQ) +
                              vol_M1 * M2_grid.eos.interpolate("muS", muB, muQ);

                double density_ = (1. - vol_M1) * M1_grid.eos.interpolate("nB", muB, muQ) +
                                  vol_M1 * M2_grid.eos.interpolate("nB", muB, muQ);

                double strangeness_density_ = (1. - vol_M1) * M1_grid.eos.interpolate("nS", muB, muQ) +
                                              vol_M1 * M2_grid.eos.interpolate("nS", muB, muQ);

                double charge_density_ = (1. - vol_M1) * M1_grid.eos.interpolate("nQ", muB, muQ) +
                                         vol_M1 * M2_grid.eos.interpolate("nQ", muB, muQ);

                double energy_density_ = (1. - vol_M1) * M1_grid.eos.interpolate("energy_density", muB, muQ) +
                                         vol_M1 * M2_grid.eos.interpolate("energy_density", muB, muQ) +
                                         leptons.eos.interpolate("energy_density", -muQ);

                double pressure_ = (1. - vol_M1) * M1_grid.eos.interpolate("pressure", muB, muQ) +
                                   vol_M1 * M2_grid.eos.interpolate("pressure", muB, muQ) +
                                   leptons.eos.interpolate("pressure", -muQ);

                double entropy_density_ = (1. - vol_M1) * M1_grid.eos.interpolate("entropy_density", muB, muQ) +
                                          vol_M1 * M2_grid.eos.interpolate("entropy_density", muB, muQ) +
                                          leptons.eos.interpolate("entropy_density", -muQ);

                std::vector<double> output_eos_vector = {temperature,
                                                         muB,
                                                         muS_,
                                                         muQ,
                                                         density_,
                                                         strangeness_density_,
                                                         charge_density_,
                                                         energy_density_,
                                                         pressure_,
                                                         entropy_density_};

                synthesized.eos.push_back(output_eos_vector);
                
                std::vector<double> output_transition_vector = output_eos_vector;

                output_transition_vector.push_back(vol_M1);
                output.transition.write(output_transition_vector);
                output.transition.skip_line();

                // size_t first_eos_file_size = M1_grid.eos.get_data()[0].size();
                // size_t second_eos_file_size = M1_grid.eos.get_data()[0].size();
                // size_t eos_file_size = std::max(first_eos_file_size, second_eos_file_size);

                // if (eos_file_size > output_eos_vector.size()) {  // 10 is the standard size
                //     double value_ = 0.;
                //     for (size_t i = output_eos_vector.size(); i < eos_file_size; i++) {
                //         if (first_eos_file_size >= i) value_ = (1. - vol_M1) * M1_grid.eos.interpolate(i, muB, muQ);
                //         if (second_eos_file_size >= i) value_ += vol_M1 * M2_grid.eos.interpolate(i, muB, muQ);
                //         output_eos_vector.push_back(value_);
                //     }
                // }

                // /> Write density output to file
                if (input_params.output_particle_properties) {
                    // First columns are T, muB, muQ
                    std::vector<double> output_particle_properties_vector = {
                        temperature,
                        muB,
                        muS_,
                        muQ,
                    };

                    size_t first_particles_file_size = M1_grid.particle_properties.get_data()[0].size();
                    size_t second_particles_file_size = M2_grid.particle_properties.get_data()[0].size();
                    size_t particles_file_size = std::max(first_particles_file_size, second_particles_file_size);

                    // Next columns are the densities
                    for (size_t i = 4; i < particles_file_size; i++) {
                        double value_ = 0.;
                        if (first_particles_file_size >= i)
                            value_ = (1. - vol_M1) * M1_grid.particle_properties.interpolate(i, muB, muQ);
                        if (second_particles_file_size >= i)
                            value_ += vol_M1 * M2_grid.particle_properties.interpolate(i, muB, muQ);

                        output_particle_properties_vector.push_back(value_);
                    }

                    size_t lepton_particles_size = leptons.particle_properties.get_data()[0].size();

                    for (size_t i = 1; i < lepton_particles_size; i++) {
                        double value_ = leptons.particle_properties.interpolate(i, -muQ);
                        output_particle_properties_vector.push_back(value_);
                    }

                    synthesized.particle_properties.push_back(output_particle_properties_vector);
                }

                // if(input_params.output_flavor_equilibration){
                //     std::vector<double> output_flavor_vector={
                //         temperature,
                //         muB,
                //         muS_,
                //         muQ,
                //     };

                //     size_t first_flavor_file_size= M1_grid.flavor_equilibration.get_data()[0].size();
                //     size_t second_flavor_file_size = M2_grid.flavor_equilibration.get_data()[0].size();
                //     size_t flavor_file_size= std::max(first_flavor_file_size, second_flavor_file_size);

                //     //Next columns are the flavor_equilibration
                //     for(size_t i=4; i<flavor_file_size; i++){
                //         double value_=0.;
                //         if(first_flavor_file_size>=i)
                //             value_= (1.-vol_M1)*M1_grid.flavor_equilibration.interpolate(i, muB, muQ);
                //         if(second_flavor_file_size>=i)
                //             value_+= vol_M1*M2_grid.flavor_equilibration.interpolate(i, muB, muQ);
                //         output_flavor_vector.push_back(value_);
                //     }

                //     size_t lepton_flavor_file_size= leptons.flavor_equilibration.get_data()[0].size();

                //     for(size_t i=0; i<lepton_flavor_file_size; i++){
                //         double value_= leptons.eos.interpolate(i, -muQ);
                //         output_flavor_vector.push_back(value_);
                //     }

                //     synthesized.flavor_equilibration.push_back(output_flavor_vector);

                // }

                // if (input_params.output_compOSE) {
                //     std::vector<double> output_compose_vector = {
                //         temperature,
                //         muB,
                //         muS_,
                //         muQ,
                //     };

                //     size_t first_compose_file = M1_grid.compOSE.get_data()[0].size();
                //     size_t second_compose_file_size = M2_grid.compOSE.get_data()[0].size();
                //     size_t compose_file_size = std::max(first_compose_file, second_compose_file_size);

                //     // Next columns are the compOSE
                //     for (size_t i = 4; i < compose_file_size; i++) {
                //         double value_ = 0.;
                //         if (first_compose_file >= i) value_ = (1. - vol_M1) * M1_grid.compOSE.interpolate(i, muB, muQ);
                //         if (second_compose_file_size >= i) value_ += vol_M1 * M2_grid.compOSE.interpolate(i, muB, muQ);
                //         output_compose_vector.push_back(value_);
                //     }

                //     size_t lepton_compose_file_size = leptons.compOSE.get_data()[0].size();

                //     for (size_t i = 0; i < lepton_compose_file_size; i++) {
                //         double value_ = leptons.eos.interpolate(i, -muQ);
                //         output_compose_vector.push_back(value_);
                //     }

                //     synthesized.compOSE.push_back(output_compose_vector);
                // }

            }  // end outputs if solution is usable
        }


        if(synthesized.eos.get_data().empty()){
            const std::string err_msg = "No solution found for the mixed phase.";
            InfoLog::getInstance().log(406, err_msg);
            StatusLog::getInstance().setCode(406);
            throw std::runtime_error(err_msg);
        }


        // Match the mixed phase w/ beta-equilibrated EoS
        M1_beta.eos.read(input_params.files_M1_beta.eos);
        M2_beta.eos.read(input_params.files_M2_beta.eos);

        M1_beta.eos.setup_keys(keys_eos);
        M2_beta.eos.setup_keys(keys_eos);
        
        if (input_params.output_particle_properties) {
            M1_beta.particle_properties.read(input_params.files_M1_beta.particle_properties);
            M2_beta.particle_properties.read(input_params.files_M2_beta.particle_properties);

            if(M1_beta.particle_properties.get_data().size() == 0 || M2_beta.particle_properties.get_data().size() == 0){
                InfoLog::getInstance().log(151, "Error with Particle properties input files. Skipping output.");
                input_params.output_particle_properties = false;
            }else{
                M1_beta.particle_properties.setup_keys(keys_shared);
                M2_beta.particle_properties.setup_keys(keys_shared);
            }
        }

        // if(input_params.output_flavor_equilibration){
        //     M1_beta.flavor_equilibration.read(input_params.files_M1_beta.flavor_equilibration);
        //     M2_beta.flavor_equilibration.read(input_params.files_M2_beta.flavor_equilibration);
        // }

        // if (input_params.output_compOSE) {
        //     M1_beta.compOSE.read(input_params.files_M1_beta.compOSE);
        //     M2_beta.compOSE.read(input_params.files_M2_beta.compOSE);

        //     if(M1_beta.compOSE.get_data().size() == 0 || M2_beta.compOSE.get_data().size() == 0){
        //         InfoLog::getInstance().log(151, "Error with compOSE input files. Skipping output.");
        //         input_params.output_compOSE = false;
        //     }else{
        //         M1_beta.compOSE.setup_keys(keys_shared);
        //         M2_beta.compOSE.setup_keys(keys_shared);
        //     }
        // }

        M1_beta.setup_data_set();
        M2_beta.setup_data_set();

        M1_beta.order_in("temperature", "muB");
        M2_beta.order_in("temperature", "muB");

        M1_beta.setup_interpolator_in("muB");
        M2_beta.setup_interpolator_in("muB");

        // attach the beta-equilibrated EoS to the mixed phase EoS

        // find the minimum and maximum values of muB in the mixed phase EoS
        double muB_transition_minimum = synthesized.eos.get_column("muB").front();
        double muB_transition_maximum = synthesized.eos.get_column("muB").back();

        // get the pressure values at the minimum and maximum values of muB in the mixed phase EoS
        double pressure_at_muB_min_M1 = M1_beta.eos.interpolate("pressure", muB_transition_minimum);
        double pressure_at_muB_min_M2 = M2_beta.eos.interpolate("pressure", muB_transition_minimum);
        double max_pressure_at_muB_min_trans = std::max(pressure_at_muB_min_M1, pressure_at_muB_min_M2);

        double pressure_at_muB_max_M1 = M1_beta.eos.interpolate("pressure", muB_transition_maximum);
        double pressure_at_muB_max_M2 = M2_beta.eos.interpolate("pressure", muB_transition_maximum);

        double max_pressure_at_muB_max_trans = std::max(pressure_at_muB_max_M1, pressure_at_muB_max_M2);

        DataSet<double> M1_beta_stable(input_params);
        DataSet<double> M2_beta_stable(input_params);

        if (are_equal(max_pressure_at_muB_min_trans, pressure_at_muB_min_M1) &&
            are_equal(max_pressure_at_muB_max_trans, pressure_at_muB_max_M2)) {
            M1_beta_stable = remove_upper("muB", muB_transition_minimum, M1_beta);
            M2_beta_stable = remove_lower("muB", muB_transition_maximum, M2_beta);

            for (auto row : M1_beta_stable.eos.get_data()) {
                synthesized.eos.push_back(row);
            }

            for (auto row : M2_beta_stable.eos.get_data()) {
                synthesized.eos.push_back(row);
            }

            if(input_params.output_particle_properties){
                for(auto row: M1_beta_stable.particle_properties.get_data()){
                    synthesized.particle_properties.push_back(row);
                }

                for(auto row: M2_beta_stable.particle_properties.get_data()){
                    synthesized.particle_properties.push_back(row);
                }
            }

            // if(input_params.output_compOSE){
            //     for(auto row: M1_beta_stable.compOSE.get_data()){
            //         synthesized.compOSE.push_back(row);
            //     }

            //     for(auto row: M2_beta_stable.compOSE.get_data()){
            //         synthesized.compOSE.push_back(row);
            //     }
            // }


        } else {
            M1_beta_stable = remove_lower("muB", muB_transition_minimum, M1_beta);
            M2_beta_stable = remove_upper("muB", muB_transition_maximum, M2_beta);

            for (auto row : M1_beta_stable.eos.get_data()) {
                synthesized.eos.push_back(row);
            }

            for (auto row : M2_beta_stable.eos.get_data()) {
                synthesized.eos.push_back(row);
            }

            if(input_params.output_particle_properties){
                for(auto row: M1_beta_stable.particle_properties.get_data()){
                    synthesized.particle_properties.push_back(row);
                }

                for(auto row: M2_beta_stable.particle_properties.get_data()){
                    synthesized.particle_properties.push_back(row);
                }
            }

            // if(input_params.output_compOSE){
            //     for(auto row: M1_beta_stable.compOSE.get_data()){
            //         synthesized.compOSE.push_back(row);
            //     }

            //     for(auto row: M2_beta_stable.compOSE.get_data()){
            //         synthesized.compOSE.push_back(row);
            //     }
            // }
            
        }

        synthesized.order_in("temperature", "muB");

        // const size_t col_size= synthesized.eos.get_data()[0].size();
        //     if(col_size > keys_eos.size()){
                
        //         for(size_t i = keys_eos.size(); i < col_size; i++){
        //         std::cout << "here?\n" << i << " " << col_size << std::endl;
        //             synthesized.eos.erase_column(i);
        //         }
                
        //     }
            
        for (auto row : synthesized.eos.get_data()) {
            output.eos.write(row);
            output.eos.skip_line();
        }

        if (input_params.output_particle_properties) {
            for (auto row : synthesized.particle_properties.get_data()) {
                output.particle_properties.write(row);
                output.particle_properties.skip_line();
            }
        }

        // if(input_params.output_flavor_equilibration){
        //     for(auto row: synthesized.flavor_equilibration.get_data()){
        //         output.flavor_equilibration.write(row);
        //         output.flavor_equilibration.skip_line();
        //     }
        // }

        // if (input_params.output_compOSE) {
        //     for (auto row : synthesized.compOSE.get_data()) {
        //         output.compOSE.write(row);
        //         output.compOSE.skip_line();
        //     }
        // }

        if (input_params.verbose) std::cout << "\nFinished first order Gibbs phase transition.\n";

        eos_processing(synthesized);
        
        output.eos.close_file();
        output.particle_properties.close_file();
        // output.flavor_equilibration.close_file();
        // output.compOSE.close_file();
        output.transition.close_file();
        output.derivatives.close_file();

    }  // end function compute_beta_equilibrium

    bool compute_gibbs_phase_transition(std::optional<double> muB_min_overlap = std::nullopt,
                                        std::optional<double> muB_max_overlap = std::nullopt,
                                        std::optional<double> muQ_min_overlap = std::nullopt,
                                        std::optional<double> muQ_max_overlap = std::nullopt) {
        double muB_init = muB;
        double muQ_init = muQ;
        double x[] = {muB_init, muQ_init};

        // Define Ceres Solver variables:
        Problem problem;

        CostFunction* cost = new NumericDiffCostFunction<Gibbs_phase_transition_functor, ceres::CENTRAL, 2, 2>(
            new Gibbs_phase_transition_functor(vol_M1, M1_grid, M2_grid, leptons));

        problem.AddResidualBlock(cost, NULL, x);

        // Only look for values of muQ between the minimum and maximum values of muQ in the input file

        if (muB_min_overlap.has_value()) problem.SetParameterLowerBound(x, 0, muB_min_overlap.value());
        if (muB_max_overlap.has_value()) problem.SetParameterUpperBound(x, 0, muB_max_overlap.value());

        if (muQ_min_overlap.has_value()) problem.SetParameterLowerBound(x, 1, muQ_min_overlap.value());
        if (muQ_max_overlap.has_value()) problem.SetParameterUpperBound(x, 1, muQ_max_overlap.value());

        // Run
        Solve(options, &problem, &summary);
        // Print if convergence was achieved (if debug)

        print_solver_log();

        muB = x[0];
        muQ = x[1];

        return (summary.IsSolutionUsable() && summary.final_cost < input_params.solver.convergence_threshold);
    }

};  // end eos_synthesis class

}  // namespace Synthesis

template <typename T>
bool Gibbs_phase_transition_functor::operator()(const T* x, T* residuals) const {
    T muB = x[0];
    T muQ = x[1];

    T pressure_1 = M1.eos.interpolate("pressure", muB, muQ);
    T pressure_2 = M2.eos.interpolate("pressure", muB, muQ);

    T charge_density_1 = M1.eos.interpolate("nQ", muB, muQ);
    T charge_density_2 = M2.eos.interpolate("nQ", muB, muQ);

    T charge_leptons = leptons.eos.interpolate("nQ", -muQ);

    residuals[0] = pressure_1 - pressure_2;
    residuals[1] = (1. - vol_M1) * charge_density_1 + vol_M1 * charge_density_2 + charge_leptons;
    return true;
}

#endif