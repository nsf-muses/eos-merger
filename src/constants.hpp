#pragma once

#ifndef SYNTHESIS_CONSTANTS_HPP
#define SYNTHESIS_CONSTANTS_HPP
#include <cmath>
#include <string>

namespace Synthesis {

inline const std::string INPUT_PATH = "../input/";
inline const std::string OUTPUT_PATH = "../output/";
constexpr int OUTPUT_PRECISION = 12;
constexpr double alpha_em = 1.0 / 137.035999139;
constexpr double e_gauss = 8.54245431146984625173e-2;  // e in gauss
constexpr double hbarc = 197.3269804;                  // hbar*c in MeV*fm
std::string OUTPUT_SEPARATOR = ",";

const int temperature_index = 0;
const int baryon_chem_pot_index = 1;
const int strange_chem_pot_index = 2;
const int charge_chem_pot_index = 3;
const int baryon_density_index = 4;
const int strange_density_index = 5;
const int charge_density_index = 6;
const int energy_density_index = 7;
const int pressure_index = 8;
const int entropy_density_index = 9;
const int baryon_density_prime_index = 10;

}  // namespace Synthesis

#endif  // CONSTANTS_HPP