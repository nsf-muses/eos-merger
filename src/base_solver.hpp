#ifndef SYNTHESIS_SOLVER_OPTIONS_HPP
#define SYNTHESIS_SOLVER_OPTIONS_HPP

#pragma once

#include <optional>

#include "ceres/ceres.h"
#include "data.hpp"
#include "glog/logging.h"
#include "input.hpp"
#include "interpolator.hpp"
#include "derivatives.hpp"
#include "output.hpp"
#include "log.hpp"
#include "utils.hpp"
using ceres::CostFunction;
using ceres::NumericDiffCostFunction;
using ceres::Problem;
using ceres::Solve;
using ceres::Solver;

namespace Synthesis {

class SynthesisMethod {
   public:
    InputParams &input_params;

    DataSet<double> M1_beta;  ///< Object to handle the Beta-equilibrium EoS from model 1.
    DataSet<double> M2_beta;  ///< Object to handle the Beta-equilibrium EoS from model 2.
    DataSet<double> M1_grid;  ///< Object to handle the EoS from model 1 (no leptons).
    DataSet<double> M2_grid;  ///< Object to handle the EoS from model 2 (no leptons).
    DataSet<double> leptons;  ///< Object to handle the EoS from model 1 (leptons).

    DataSet<double> synthesized;  ///< Object to handle the final EoS

    Output output;  ///< Object to manage various output files.

    Solver::Options options;  ///< Options for the solver.
    Solver::Summary summary;  ///< Summary of the solver.

    bool first_run;                   ///< Flag to indicate if the solver is running for the first time.

    std::vector<std::pair<size_t, std::string>> keys_shared;
    std::vector<std::pair<size_t, std::string>> keys_eos;
    std::vector<std::pair<size_t, std::string>> keys_leptons;
    std::vector<std::pair<size_t, std::string>> keys_derivatives;

    SynthesisMethod(InputParams &input_params_)
        : input_params(input_params_),
          M1_beta(input_params_),
          M2_beta(input_params_),
          M1_grid(input_params_),
          M2_grid(input_params_),
          leptons(input_params_),
          synthesized(input_params_),
          output(input_params_),
          first_run(true) {

                keys_shared = {{0, "temperature"}, {1, "muB"}, {2, "muS"}, {3, "muQ"}, {4, "nB"}, {5, "nS"}, {6, "nQ"}};

                keys_eos = {
                    {0, "temperature"}, {1, "muB"},
                    {2, "muS"},         {3, "muQ"},
                    {4, "nB"},          {5, "nS"},
                    {6, "nQ"},          {7, "energy_density"},
                    {8, "pressure"},    {9, "entropy_density"},
                };


                keys_leptons = {
                    {0, "temperature"}, {1, "muE"},
                    {2, "muNu"},        {3, "nL"},
                    {4, "nQ"},          {5, "energy_density"},
                    {6, "pressure"},    {7, "entropy_density"},
                };

                keys_derivatives = {
                    {0, "temperature"}, {1, "muB"},
                    {2, "muS"},         {3, "muQ"},
                    {4, "nB"},          {5, "nS"},
                    {6, "nQ"},          {7, "cs2"},
                    {8, "chi1"},    {9, "chi2"},
                    {10, "chi3"}
                };


    }

    ~SynthesisMethod() {
        output.eos.close_file();
        output.transition.close_file();
        output.particle_properties.close_file();
        // output.flavor_equilibration.close_file();
        // output.compOSE.close_file();
        output.derivatives.close_file();
    }


    void setup_solver_options() {
        first_run = true;

        validate_solver_options();
        options.parameter_tolerance = input_params.solver.parameter_tolerance;
        options.function_tolerance = input_params.solver.function_tolerance;
        options.gradient_tolerance = input_params.solver.gradient_tolerance;
        options.use_nonmonotonic_steps = input_params.solver.use_nonmonotonic_steps;
        options.max_num_iterations = input_params.solver.max_iterations;

        options.update_state_every_iteration = true;
        options.num_threads =  std::thread::hardware_concurrency();
        options.max_num_consecutive_invalid_steps= 10;

        if (input_params.solver.method == "levenbergMarquardt") {
            options.trust_region_strategy_type = ceres::LEVENBERG_MARQUARDT;
        } else if (input_params.solver.method == "dogleg") {
            options.trust_region_strategy_type = ceres::DOGLEG;
        } else if (input_params.solver.method == "bfgs") {
            options.line_search_direction_type = ceres::BFGS;
        } else if (input_params.solver.method == "steepestDescent") {
            options.line_search_direction_type = ceres::STEEPEST_DESCENT;
        }else if (input_params.solver.method == "lbfgs") {
            options.line_search_direction_type = ceres::LBFGS;
        }else if (input_params.solver.method == "nonlinearConjugateGradient") {
            options.line_search_direction_type = ceres::NONLINEAR_CONJUGATE_GRADIENT;   
        } else {
            StatusLog::getInstance().setCode(400);
            throw std::invalid_argument("Invalid solver method specified: " + input_params.solver.method);
        }


        if (input_params.solver.linear_solver == "denseQR") {
            options.linear_solver_type = ceres::DENSE_QR;
        } else if (input_params.solver.linear_solver == "sparseSchur") {
            options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
        } else if (input_params.solver.linear_solver == "denseNormalCholesky") {
            options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
        } else if (input_params.solver.linear_solver == "iterativeSchur") {
            options.linear_solver_type = ceres::ITERATIVE_SCHUR;
        } else {
            StatusLog::getInstance().setCode(400);
            throw std::invalid_argument("Invalid linear solver type specified: " + input_params.solver.linear_solver);
        }   


        // Set CERES verbose level
        if (input_params.verbose <= 2)
            FLAGS_minloglevel = google::FATAL;
        else if (input_params.verbose == 3)
            FLAGS_minloglevel = google::ERROR;
        else if (input_params.verbose == 4)
            FLAGS_minloglevel = google::WARNING;

        options.minimizer_progress_to_stdout = input_params.verbose > 2 ? true : false;
    }

    void validate_solver_options() {
        if ((input_params.solver.method == "bfgs" || input_params.solver.method == "steepestDescent" ||
             input_params.solver.method == "nonlinearConjugateGradient") &&
            (input_params.solver.linear_solver == "sparseSchur" || input_params.solver.linear_solver == "iterativeSchur")) {
            
            StatusLog::getInstance().setCode(400);
            throw std::runtime_error("Line search methods are incompatible with sparse or iterative solvers.");
        }

        if (input_params.solver.method == "dogleg" && input_params.solver.linear_solver == "iterativeSchur") {
            StatusLog::getInstance().setCode(400);
            throw std::runtime_error("Dogleg trust region strategy is incompatible with iterative solvers.");
        }
    }


    virtual void compute_synthesis() = 0;


    void print_solver_log() {
        if (input_params.verbose == 2 || input_params.verbose == 3)
            std::cout << summary.BriefReport() << "\n";
        else if (input_params.verbose > 3)
            std::cout << summary.FullReport() << "\n";
    }



    template <typename T>
    DataSet<T> remove_upper(const std::string &key, T value_, DataSet<T> data_, bool exclude_equal = false) {
        
        data_.eos.order_in(key);

        DataSet<T> sliced_data(input_params);

        auto key_vals = data_.eos.get_column(key);

        // Use std::lower_bound to find the first element greater than or equal to value_
        auto i = exclude_equal
                        ? std::upper_bound(key_vals.begin(), key_vals.end(), value_)   // includes equal values
                        : std::lower_bound(key_vals.begin(), key_vals.end(), value_);  // excludes equal values


        auto k = i - key_vals.begin() - 1;

        // Ensure k is not less than zero
        size_t k_cast = (k >= 0) ? static_cast<size_t>(k) : 0;

        if (k_cast < key_vals.size() && k_cast > 0) {
            for (size_t idx = 0; idx <= k_cast; ++idx) {
                
                sliced_data.eos.push_back(data_.eos.get_data()[idx]);

                if (input_params.output_particle_properties)
                    sliced_data.particle_properties.push_back(data_.particle_properties.get_data()[idx]);

                if(input_params.output_derivatives && !data_.derivatives.get_data().empty())
                    sliced_data.derivatives.push_back(data_.derivatives.get_data()[idx]);
                // if(input_params.output_flavor_equilibration )
                //     sliced_data.flavor_equilibration.push_back(data_.flavor_equilibration.get_data()[idx]);

                // if (input_params.output_compOSE) sliced_data.compOSE.push_back(data_.compOSE.get_data()[idx]);
            }
        } else {
            // Handle the case where the critical value is at or beyond the end of the range
            // std::cout << "Critical value not within or beyond the columns." << std::endl;
            // std::throw(std::runtime_error("Critical value not within or beyond the columns."));
        }

        return sliced_data;
    }


    template <typename T>
    DataSet<T> remove_lower(const std::string &key, double value_, DataSet<T> data_, bool exclude_equal = false) {
        data_.eos.order_in(key);
        DataSet<T> sliced_data(input_params);

        auto key_vals = data_.eos.get_column(key);
        auto i = exclude_equal 
                    ? std::upper_bound(key_vals.begin(), key_vals.end(), value_) 
                    : std::lower_bound(key_vals.begin(), key_vals.end(), value_);


        auto k = i - key_vals.begin();

        size_t k_cast = static_cast<size_t>(k);
        if (k_cast < key_vals.size()-1) {
            for (size_t idx = k_cast; idx < data_.eos.get_data().size(); ++idx) {
                sliced_data.eos.push_back(data_.eos.get_data()[idx]);

                if (input_params.output_particle_properties)
                    sliced_data.particle_properties.push_back(data_.particle_properties.get_data()[idx]);

                if(input_params.output_derivatives && !data_.derivatives.get_data().empty())
                    sliced_data.derivatives.push_back(data_.derivatives.get_data()[idx]);

                // if(input_params.output_flavor_equilibration )
                //     sliced_data.flavor_equilibration.push_back(data_.flavor_equilibration.get_data()[idx]);

                // if (input_params.output_compOSE) sliced_data.compOSE.push_back(data_.compOSE.get_data()[idx]);

            }
        } else {
            // Handle the case where the critical value is at or beyond the end of the range
            // std::cout << "Critical value not within or beyond the columns." << std::endl;
            // std::throw(std::runtime_error("Critical value not within or beyond the columns."));
        }

        return sliced_data;
    }

    template <typename T>
    DataSet<T> join(DataSet<T> &M1, DataSet<T> &M2) {
        DataSet<T> M_joined(input_params);
        for (size_t idx = 0; idx < M1.eos.get_data().size(); ++idx) {
            M_joined.eos.push_back(M1.eos.get_data()[idx]);

            if (input_params.output_particle_properties && !M1.particle_properties.get_data()[idx].empty())
                M_joined.particle_properties.push_back(M1.particle_properties.get_data()[idx]);

            if(input_params.output_derivatives && !M1.derivatives.get_data().empty())
                M_joined.derivatives.push_back(M1.derivatives.get_data()[idx]);
            // if(input_params.output_flavor_equilibration && !M1.flavor_equilibration.get_data()[idx].empty())
            // M_joined.flavor_equilibration.push_back(M1.flavor_equilibration.get_data()[idx]);
            // if (input_params.output_compOSE && !M1.compOSE.get_data()[idx].empty())
            //     M_joined.compOSE.push_back(M1.compOSE.get_data()[idx]);
        }

        for (size_t idx = 0; idx < M2.eos.get_data().size(); ++idx) {
            M_joined.eos.push_back(M2.eos.get_data()[idx]);

            if (input_params.output_particle_properties && !M2.particle_properties.get_data()[idx].empty())
                M_joined.particle_properties.push_back(M2.particle_properties.get_data()[idx]);

            if(input_params.output_derivatives && !M2.derivatives.get_data().empty())
                M_joined.derivatives.push_back(M2.derivatives.get_data()[idx]);
            // if(input_params.output_flavor_equilibration && !M2.flavor_equilibration.get_data()[idx].empty())
            //     M_joined.flavor_equilibration.push_back(M2.flavor_equilibration.get_data()[idx]);

            // if (input_params.output_compOSE && !M2.compOSE.get_data()[idx].empty())
            //     M_joined.compOSE.push_back(M2.compOSE.get_data()[idx]);
        }

        return M_joined;
    }


    void check_input_data(){


        if (input_params.verbose) {
            std::cout << "# of lines in the first eos file: " <<  M1_beta.eos.get_data().size() << std::endl;
            std::cout << "# of lines in the second eos file: " << M2_beta.eos.get_data().size() << std::endl;
        }
        
        if (M1_beta.eos.get_data().empty() || M2_beta.eos.get_data().empty()) {
            StatusLog::getInstance().setCode(400);
            throw std::runtime_error("Error reading EoS files. One or both files are empty. Exiting.");
        }
    }


    DataContainer<double> compute_derivatives(DataContainer<double> &data) {
        if (input_params.verbose) std::cout << "\nCalculating derivatives\n";

        DataContainer<double> diff;

        try{

            std::unique_ptr<DerivativeStrategy> strategy;


            if (input_params.derivatives_method == "finite_difference") {
                strategy = std::make_unique<FiniteDifferenceDerivative>(data, input_params);
            } else if (input_params.derivatives_method == "gsl") {
                strategy = std::make_unique<GSLDerivative>(data, input_params);
            } else {
                throw std::invalid_argument("Unknown derivatives method specified in input_params.");
            }

            Derivatives derivatives(std::move(strategy));

            derivatives.set_x_variable("nB");



            for (size_t i = 0; i < data.get_data().size(); i++) {

                std::vector<double> derivatives_row = {
                    data.get_data()[i][temperature_index],      data.get_data()[i][baryon_chem_pot_index],
                    data.get_data()[i][strange_chem_pot_index], data.get_data()[i][charge_chem_pot_index],
                    data.get_data()[i][baryon_density_index],   data.get_data()[i][strange_density_index],
                    data.get_data()[i][charge_density_index]};


               
                double cs2 = 
                // (input_params.synthesis_type == "hyperbolic-tangent" 
                //                 && input_params.y_var_interp_method == "speed_of_sound_squared")
                                // ? data.get_data()[i].back() 
                                // :
                                 (derivatives.get_first_derivative("pressure", data.get_data()[i][baryon_density_index]) /
                             derivatives.get_first_derivative("energy_density", data.get_data()[i][baryon_density_index]));

                 double chi1_muB =derivatives.get_first_derivative("pressure", data.get_data()[i][baryon_density_index])
                                                    /derivatives.get_first_derivative("muB", data.get_data()[i][baryon_density_index]);


                
                derivatives_row.push_back(cs2);
                derivatives_row.push_back(chi1_muB);
                diff.push_back(derivatives_row);
            }


            diff.setup_keys(keys_derivatives);


            std::unique_ptr<DerivativeStrategy> new_strategy;


            if (input_params.derivatives_method == "finite_difference") {
                new_strategy = std::make_unique<FiniteDifferenceDerivative>(diff, input_params);
            } else if (input_params.derivatives_method == "gsl") {
                new_strategy = std::make_unique<GSLDerivative>(diff, input_params);
            } else {
                throw std::invalid_argument("Unknown derivatives method specified in input_params.");
            }

            Derivatives higher_order_derivatives(std::move(new_strategy));


            diff.order_in("muB");
            higher_order_derivatives.set_x_variable("muB");

            for (size_t i = 0; i < diff.get_data().size(); i++) {
                double chi2_muB = higher_order_derivatives.get_first_derivative("chi1", data.get_data()[i][baryon_chem_pot_index]);
                double chi3_muB = higher_order_derivatives.get_second_derivative("chi1", data.get_data()[i][baryon_chem_pot_index]);


                chi2_muB*= hbarc*hbarc*hbarc; // MeV^2
                chi3_muB*= hbarc*hbarc*hbarc; // MeV

                diff.push_back(chi2_muB, i);
                diff.push_back(chi3_muB, i);


            }

            if (input_params.verbose) std::cout << "Done!\n\n";

        } catch (const std::exception& e) {
            InfoLog::getInstance().log(101, "Error in compute_derivatives: " + std::string(e.what()));
        }

        return diff;

    }



    void check_stability(DataSet<double> &data) {
        if (input_params.verbose) std::cout << "\nChecking EoS stability\n";

        try{
            // important: for multidimensional EoS, the order of the columns is important for the output
            data.eos.order_in("nB");

            for (size_t i = 0; i < data.eos.get_data().size() - 1; i++) {
                
                bool is_stable_local = true;

                if (data.eos.get_data()[i][baryon_chem_pot_index] > data.eos.get_data()[i + 1][baryon_chem_pot_index]
                //For very low densities/crust the interpolation can give off instabilities in the chemical potential due to its steep rise 
                && data.eos.get_data()[i][baryon_density_index] > 1e-5 && !are_equal(data.eos.get_data()[i][baryon_chem_pot_index], data.eos.get_data()[i + 1][baryon_chem_pot_index], 1e-6))
                    is_stable_local = false;

                if (data.eos.get_data()[i][pressure_index] > data.eos.get_data()[i + 1][pressure_index]
                && !are_equal(data.eos.get_data()[i][pressure_index], data.eos.get_data()[i + 1][pressure_index], 1e-6))
                    is_stable_local = false;

                if (data.eos.get_data()[i][pressure_index] < 0.)
                    is_stable_local = false;
                

                if(!is_stable_local){
                    data.stable = false;

                    std::ostringstream oss;
                    oss << std::fixed << std::setprecision(13) << data.eos.get_data()[i][temperature_index]
                        << " MeV, " << data.eos.get_data()[i][baryon_density_index] << " fm-3";

                    InfoLog::getInstance().log(101, "EoS is not stable at (T, nB)= " + oss.str());

                }

            }

             if (data.stable) {
                    InfoLog::getInstance().log(100, "EoS is stable");
                    if (input_params.verbose) std::cout << "EoS is stable\n\n";
                }else{
                    InfoLog::getInstance().log(101, "EoS is not stable");
                    if (input_params.verbose) std::cout << "EoS is not stable\n\n";
                }

        } catch (const std::exception& e) {
            InfoLog::getInstance().log(101, "Error in check_stability: " + std::string(e.what()));
        }

    }

    void check_eos_causality(DataSet<double> &data){

        bool is_superluminal = false;

        try{
            int cs2_index=7;
            for(size_t i=0; i< data.derivatives.get_data().size(); i++){
                if(data.derivatives.get_data()[i][cs2_index] > 1.){
                    is_superluminal = true;

                    std::ostringstream oss;
                    oss << std::fixed << std::setprecision(13) << data.derivatives.get_data()[i][temperature_index]
                        << " MeV, " << data.derivatives.get_data()[i][baryon_density_index] << " fm-3";

                    InfoLog::getInstance().log(101, "cs2 is superluminal at (T, nB)= " + oss.str());
                }

                if(data.derivatives.get_data()[i][cs2_index]<0.){
                    data.stable = false;
                   std::ostringstream oss;
                    oss << std::fixed << std::setprecision(13) << data.derivatives.get_data()[i][temperature_index]
                        << " MeV, " << data.derivatives.get_data()[i][baryon_density_index] << " fm-3";

                    InfoLog::getInstance().log(101, "cs2 is negative at (T, nB)= " + oss.str());
                }
            }


            if (is_superluminal) {
               InfoLog::getInstance().log(100, "EoS is superluminal");
               if (input_params.verbose) std::cout << "EoS is superluminal\n\n";
            }

        } catch (const std::exception& e) {
            InfoLog::getInstance().log(101, "Error checking EoS stability & causality: " + std::string(e.what()));
        }
    }


    std::string normalize_input_key(const std::string& key) {
        if (key == "baryon_chemical_potential") return "muB";
        if (key == "charge_chemical_potential") return "muQ";
        if (key == "strange_chemical_potential") return "muS";
        if (key == "baryon_density") return "nB";
        if (key == "charge_density") return "nQ";
        if (key == "strange_density") return "nS";
        // else return the original key
        return key;
    }


    virtual void eos_processing(DataSet<double> &data) {

        if (input_params.output_derivatives || input_params.check_eos_stability) {

            // Stability check
            if (input_params.check_eos_stability) {
                check_stability(data);

                if (!data.is_stable()) {
                    StatusLog::getInstance().setCode(406);
                    throw std::runtime_error("The interpolated EoS is not stable. Exiting.");
                }
            }

            // Output derivatives
            if (input_params.output_derivatives && data.is_stable()) {
                data.derivatives = compute_derivatives(data.eos);
                data.derivatives.setup_keys(keys_derivatives);

                data.derivatives.order_in("nB");

                for (const auto& row : data.derivatives.get_data()) {
                    output.derivatives.write(row);
                    output.derivatives.skip_line();
                }
            }

            // Causality check
            if (input_params.check_eos_causality) {
                check_eos_causality(data);

                if (!data.is_causal()) {
                    StatusLog::getInstance().setCode(406);
                    throw std::runtime_error("The interpolated EoS is not causal. Exiting.");
                }
            }
        }
    }

    void check_temperature_values(){
        
        if (M1_beta.eos.get_unique_values_of("temperature").size() > 1 ||
            M2_beta.eos.get_unique_values_of("temperature").size() > 1) {

            StatusLog::getInstance().setCode(400); 
            throw std::runtime_error("Module cannot handle multiple temperatures. Exiting.");
        }

        
        double temp_1= M1_beta.eos.get_data()[0][0];
        double temp_2= M2_beta.eos.get_data()[0][0];

        if(!are_equal(temp_1, temp_2)){
            double replace_temp = std::min(temp_1, temp_2);
            M1_beta.eos.replace_values("temperature", replace_temp);
            M2_beta.eos.replace_values("temperature", replace_temp);    
        }      
    }


};

}  // namespace Synthesis
#endif