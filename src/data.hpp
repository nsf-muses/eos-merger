#pragma once

#include <algorithm>
#include <map>
#include <memory>
#include <set>
#include <stdexcept>
#include <tuple>
#include <unordered_map>
#include <sstream>

#include "constants.hpp"
#include "input.hpp"
// #include "derivatives.hpp"
#include "interpolator.hpp"
#include "log.hpp"
#include "utils.hpp"

namespace Synthesis {

template <typename T>
class DataContainer {
   private:
    std::vector<std::vector<T>> data;
    Interpolator interp;
    std::map<std::string, size_t> index_map;
    std::vector<std::string> interpolator_variable;

   public:
    DataContainer() {}

    /** The definitions below are necessary for correct usage
     * of std::reference_wrapper with std::vector in DataSet class
     */

    // copy constructor
    DataContainer(const DataContainer& other)
        : data(other.data),
          interp(other.interp),
          index_map(other.index_map),
          interpolator_variable(other.interpolator_variable)
          {}

    // move constructor: transfer ownership
    DataContainer(DataContainer&& other) noexcept
        : data(std::move(other.data)),
          interp(std::move(other.interp)),
          index_map(std::move(other.index_map)),
          interpolator_variable(std::move(other.interpolator_variable))
          {}

    // copy assignment operator
    DataContainer& operator=(const DataContainer& other) {
        if (this != &other) {
            data = other.data;
            interp = other.interp;
            index_map = other.index_map;
            interpolator_variable = other.interpolator_variable;
        }
        return *this;
    }

    // move assignment operator
    DataContainer& operator=(DataContainer&& other) noexcept {
        if (this != &other) {
            data = std::move(other.data);
            interp = std::move(other.interp);
            index_map = std::move(other.index_map);
            interpolator_variable = std::move(other.interpolator_variable);
        }
        return *this;
    }

    void clear() {
        data.clear();
        interp.clear();
        index_map.clear();
        interpolator_variable.clear();
    }

    // Index Map:
    void setup_keys(const std::vector<std::pair<size_t, std::string>>& keys) {
        index_map.clear();
        for (auto& key : keys) {
            index_map[key.second] = key.first;
        }
    }

    size_t get_index(const std::string& key) const {
        auto it = index_map.find(key);
        if (it == index_map.end()) {
            StatusLog::getInstance().setCode(400);
            throw std::runtime_error("Key " + key + " not found in index map.");
        }

        return it->second;
    }

    const std::map<std::string, size_t>& get_index_map() const { return index_map; }

    const Interpolator& get_interpolator() const { return interp; }

    void reset_interpolator() { interp.clear(); }

    void set_data(const std::vector<std::vector<T>>& data_) { data = data_; }

    const std::vector<std::vector<T>>& get_data() const { return data; }

    void push_back(const std::vector<T>& point) {
        if (!data.empty() && point.size() != data.front().size()) {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Data point size mismatch in push_back.");
        }
        data.push_back(point);
    }

    void push_back(const T& val, size_t index) {
        if (index >= data.size()) {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Index out of bounds in push_back.");
        }

        data[index].push_back(val);
    }

    void read(const std::string& file) { data = read_from<T>(file); }

    void order_in(std::string x_key, std::optional<std::string> y_key = std::nullopt,
                  std::optional<std::string> z_key = std::nullopt, std::optional<std::string> w_key = std::nullopt) {
        int idx1 = get_index(x_key);
        int idx2 = y_key.has_value() ? get_index(y_key.value()) : -1;
        int idx3 = z_key.has_value() ? get_index(z_key.value()) : -1;
        int idx4 = w_key.has_value() ? get_index(w_key.value()) : -1;
        
        try{
            this->data = order_data<T>(data, idx1, idx2, idx3, idx4);
        } catch (const std::exception& e) {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Error ordering data. " + std::string(e.what()));
        }
    }

    void replace_values(const std::string & key, T value){
        size_t index = get_index(key);
        replace_values(index, value);
    }

    void replace_values(const size_t &index, T value){
        for(auto& row : data){
            row[index] = value;
        }

    }

    void setup_interpolator_in(const std::string& x_key, std::optional<std::string> y_key = std::nullopt,
                               std::optional<std::string> z_key = std::nullopt) {
        interp.clear();
        interpolator_variable.clear();

        auto ix = get_index(x_key);
        interpolator_variable.push_back(x_key);

        std::optional<size_t> iy = y_key ? std::optional<size_t>(get_index(y_key.value())) : std::nullopt;
        if (y_key.has_value()) {
            interpolator_variable.push_back(y_key.value());
        }

        std::optional<size_t> iz = z_key ? std::optional<size_t>(get_index(z_key.value())) : std::nullopt;
        if (z_key.has_value()) {
            interpolator_variable.push_back(z_key.value());
        }

        try{
            interp.set_interpolator(data, ix, iy, iz);
        } catch (const std::exception& e) {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Error setting up interpolator. " + std::string(e.what()));
        }
    }


    const std::vector<std::string>& get_interpolator_variables() const { return interpolator_variable; }

    double interpolate(const std::string& key, double x, std::optional<double> y = std::nullopt,
                       std::optional<double> z = std::nullopt) const {
        size_t index = get_index(key);
        //avoid compiler warnings
        std::optional<double> safe_y = y;
        std::optional<double> safe_z = z;

        return interp(index, x, safe_y, safe_z);
    }

    double interpolate(size_t col_index, double x, std::optional<double> y = std::nullopt,
                       std::optional<double> z = std::nullopt) const {
        //avoid compiler warnings
        std::optional<double> safe_y = y;
        std::optional<double> safe_z = z;

        return interp(col_index, x, safe_y, safe_z);
    }

    std::vector<T> get_column(const std::string& key) {
        size_t index = get_index(key);
        std::vector<T> column = get_column_from_matrix<T>(data, index);

        return column;
    }


    void remove_nan_rows(const std::string &key) {
        size_t index = get_index(key);
        remove_nan_rows(index);
    
    }

    void remove_nan_rows(int index) {
        data.erase(
            std::remove_if(data.begin(), data.end(),
                [index](const std::vector<double>& row) { return std::isnan(row[index]); }),
            data.end());

    }

    void erase_column(int col_index) {
        for (auto& row : data) {
            row.erase(row.begin() + col_index);
        }
    }

    void erase_row(int row_index) {
        if (row_index >= 0 &&  static_cast<size_t>(row_index) < data.size()) {
            data.erase(data.begin() + row_index);
        }
    }

    /** @brief Get the unique values of a column in the dataset
     *   @param key key of the column
     *   @return vector of unique values
     **/
    std::vector<T> get_unique_values_of(const std::string& key) {
        std::vector<T> column = get_column(key);
        std::set<T> uniqueValues(column.begin(), column.end());
        std::vector<T> vector_unique(uniqueValues.begin(), uniqueValues.end());

        return vector_unique;
    }

    void erase_rows_with_diff_vals(const std::string& key, double value_) {
        try {
            size_t col_index = get_index(key);

            std::multimap<double, size_t> _index_map;

            for (size_t i = 0; i < data.size(); ++i) {
                _index_map.insert({data[i][col_index], i});
            }

            auto range = _index_map.equal_range(value_);

            std::vector<std::vector<double>> reduced_data;

            if (range.first == range.second) {
                data = std::vector<std::vector<double>>();
            }

            for (auto it = range.first; it != range.second; ++it) {
                std::vector<double> row(data[it->second].begin(), data[it->second].end());
                reduced_data.push_back(row);
            }

             if (reduced_data.size() != data.size()) {
                data = reduced_data;
            }

        } catch (const std::exception& e) {
            StatusLog::getInstance().setCode(500);
            throw std::runtime_error("Error erasing rows with different values. " + std::string(e.what()));
        }
    }
};


template <typename T>
class DataSet {
   private:
    InputParams& input_params;
    std::vector<std::reference_wrapper<DataContainer<T>>> data_set;

   public:
    bool stable, causal;
    
    DataContainer<T> eos;
    DataContainer<T> particle_properties;
    // DataContainer<T> flavor_equilibration;
    // DataContainer<T> compOSE;
    DataContainer<T> derivatives;


    DataSet(InputParams& input_params_) : input_params(input_params_), stable(true), causal(true) { setup_data_set(); }

    DataSet(const DataSet& other)
        : input_params(other.input_params),
          stable(other.stable),
          causal(other.causal),
          eos(other.eos),
          particle_properties(other.particle_properties),
          // flavor_equilibration(other.flavor_equilibration),
        //   compOSE(other.compOSE),
          derivatives(other.derivatives) {
        setup_data_set();
    }

    DataSet(DataSet&& other) noexcept
        : input_params(other.input_params),
          stable(std::move(other.stable)),
          causal(std::move(other.causal)),
          eos(std::move(other.eos)),
          particle_properties(std::move(other.particle_properties)),
          derivatives(std::move(other.derivatives))
          //   flavor_equilibration(std::move(other.flavor_equilibration)),
        //   compOSE(std::move(other.compOSE)) 
    {
        setup_data_set();
    }

    DataSet& operator=(const DataSet& other) {
        if (this != &other) {
            input_params = other.input_params;
            stable = other.stable;
            causal = other.causal;
            eos = other.eos;
            particle_properties = other.particle_properties;
            derivatives= other.derivatives;
            // flavor_equilibration = other.flavor_equilibration;
            // compOSE = other.compOSE;
            setup_data_set();
        }
        return *this;
    }

    DataSet& operator=(DataSet&& other) noexcept {
        if (this != &other) {
            input_params = other.input_params;
            stable = std::move(other.stable);
            causal = std::move(other.causal);
            eos = std::move(other.eos);
            particle_properties = std::move(other.particle_properties);
            derivatives = std::move(other.derivatives);
            // flavor_equilibration = std::move(other.flavor_equilibration);
            // compOSE = std::move(other.compOSE);
            setup_data_set();
        }
        return *this;
    }

    ~DataSet() = default;

    bool is_stable() const { return stable; }

    bool is_causal() const { return causal; }

    void clear() {
        eos.clear();
        particle_properties.clear();
        // flavor_equilibration.clear();
        // compOSE.clear();
        derivatives.clear();
        stable = true;
        causal = true;
    }

    void setup_data_set() {
        data_set.clear();

        data_set.push_back(std::ref(eos));

        if (input_params.output_particle_properties) {
            data_set.push_back(std::ref(particle_properties));
        }

        // if(input_params.output_derivatives){
        //     data_set.push_back(std::ref(derivatives));
        // }

        // if (input_params.output_flavor_equilibration) {
        // data_set.push_back(std::ref(flavor_equilibration));
        // }
        // if (input_params.output_compOSE) {
        //     data_set.push_back(std::ref(compOSE));
        // }
    }

    void erase_rows_with_diff_vals(const std::string& key, double value_) {
        for (auto& model : data_set) {
            model.get().erase_rows_with_diff_vals(key, value_);
        }
    }

    void setup_interpolator_in(const std::string& x_key, std::optional<std::string> y_key = std::nullopt,
                               std::optional<std::string> z_key = std::nullopt) {
        for (auto& model : data_set) {
            model.get().setup_interpolator_in(x_key, y_key, z_key);
        }
    }

    void reset_interpolator() {
        for (auto& model : data_set) {
            model.get().reset_interpolator();
        }
    }

    void order_in(const std::string& x_key, std::optional<std::string> y_key = std::nullopt,
                  std::optional<std::string> z_key = std::nullopt, std::optional<std::string> w_key = std::nullopt) {
        for (auto& model : data_set) {
            model.get().order_in(x_key, y_key, z_key, w_key);
        }
    }


    void erase_column(int col_index) {
        for (auto& model : data_set) {
            model.get().erase_column(col_index);
        }
    }

    void erase_row(int row_index) {
        for (auto& model : data_set) {
            model.get().erase_row(row_index);
        }
    }

};

}  // namespace Synthesis