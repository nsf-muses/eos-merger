#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import yaml

from muses_porter import Porter
from compose import Compose


# from muses_compose import compOSE

"""
file: postprocess.py
purpose: convert output to hdf5; make compOSE output
"""

DEFAULT_API_FILE_PATH = os.path.join(os.path.dirname(__file__), '..', 'api', 'OpenAPI_Specifications_synthesis.yaml')

def write_to_status_file(status_value, file_path, message):
    yaml_content = f"""code: {status_value}
        message: {message}
        """
    try:
        with open(file_path, 'a') as file:
            file.write(yaml_content + "\n") 
    except IOError:
        raise RuntimeError("Error: Cannot find file.") from None


def load_yaml_data(file_path):
    with open(file_path, "r") as file:
        return yaml.safe_load(file)

def parse_arguments():
    parser = argparse.ArgumentParser(description="Define the input directory with the config.yaml file.")
    parser.add_argument(
        "--config_file_path",
        type=str,
        default=os.path.join(os.path.dirname(__file__), '..', 'input'),
        help="Input folder to process. If none is provided, the default folder will be used.",
    )
    return parser.parse_args()



def main():

    args = parse_arguments()
    config_path= os.path.join(args.config_file_path, "config.yaml")
    input_params= load_yaml_data(config_path)
    csv_delimiter = ","
    
    
    output_hdf5= input_params["output"]["output_hdf5"]
    if output_hdf5:

        porter = Porter()

        output_directory_run = os.path.join("..", "output", input_params["global"]["run_name"])

        if not os.path.exists(output_directory_run):
            os.makedirs(output_directory_run)


    
        porter.import_table(
                os.path.join(output_directory_run, 'eos.csv'),
                extension="csv",
                dropna=True,
                delimiter=csv_delimiter,
                verbose=False,
                filename_schema=DEFAULT_API_FILE_PATH,
                schema="eos",
        )

        porter.export_table(
            os.path.join(output_directory_run, f"eos.h5"),
            filename_schema="../api/OpenAPI_Specifications_synthesis.yaml",
            schema="eos",
            extension="HDF5",
            dropna=True,
            verbose=False,
        )


        if input_params["output"]["output_derivatives"]:
            porter.import_table(
                os.path.join(output_directory_run, 'eos_derivatives.csv'),
                extension="csv",
                dropna=True,
                delimiter=csv_delimiter,
                verbose=False,
                filename_schema=DEFAULT_API_FILE_PATH,
                schema="derivatives",
            )
    
            porter.export_table(
                os.path.join(output_directory_run, 'eos_derivatives.h5'),
                filename_schema=DEFAULT_API_FILE_PATH,
                schema="derivatives",
                extension="HDF5",
                dropna=True,
                verbose=False,
            )          

    if input_params['output']['output_compOSE']:
        comp= Compose()
        comp.set_thermo(os.path.join("..","output", input_params['global']['run_name'],"eos.csv"))

        comp.set_varNames(['baryon_density'])
        comp.set_pts([input_params['compOSE_options']['baryon_density_points']])
        comp.set_spacings([input_params['compOSE_options']['baryon_density_spacing']])
        comp.set_default_mins_maxs()
    
        comp.interpolate()

        comp.compose_output_thermo(os.path.join("..", "output", input_params['global']['run_name'], "compOSE/"))  
    


if __name__ == "__main__":
    print("\nStarting execution of postprocess.py...")
    main()
    print("\nExecution of postprocess.py completed.")
    