#pragma once

#include <dirent.h>
#include <sys/types.h>

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "yaml-cpp/yaml.h"

namespace Synthesis {

inline bool file_exists(const std::string& fname) {
    if (FILE* file = fopen(fname.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}

template <typename T>
inline bool are_equal(T a, T b, T epsilon = 1.e-15) {
    return fabs(a - b) < epsilon ? true : false;
}

template <class C, typename T>
inline bool contains(const C& c, const T& e) {
    return std::find(std::begin(c), std::end(c), e) != std::end(c);
};

template <typename T>
inline void print_vector(const std::vector<T>& vec) {
    std::cout << "size: " << vec.size() << std::endl;
    for (const auto& val : vec) std::cout << val << " ";
    std::cout << std::endl;
}

inline void copy_file(const std::string& input_file, const std::string& output_path, const std::string& new_name = "") {
    std::filesystem::path sourcePath(input_file);
    std::filesystem::path destinationPath(output_path);

    if (!new_name.empty()) destinationPath.replace_filename(new_name);

    std::error_code ec;  // check for errors

    std::filesystem::copy(sourcePath, destinationPath, std::filesystem::copy_options::overwrite_existing, ec);

    if (ec) {
        std::cerr << "Error copying <" + input_file + "> : " << ec.message() << std::endl;
    }
}

template <typename T>
inline std::vector<T> get_unique_values_in_vector(const std::vector<T>& input_vector) {
    std::set<T> unique_values(input_vector.begin(), input_vector.end());
    return {unique_values.begin(), unique_values.end()};
}

template <typename T>
inline bool is_grid_regular(const std::vector<T>& grid) {
    if (grid.size() < 2) {
        return true;
    }
    const T diff = grid[1] - grid[0];
    const size_t grid_size = grid.size();

    for (size_t i = 1; i < grid_size; ++i) {
        if (!are_equal(grid[i] - grid[i - 1], diff)) {
            return false;
        }
    }
    return true;
}

template <typename T>
inline std::vector<std::vector<T>> read_from(const std::string& file_path) {
    std::ifstream file(file_path);
    std::vector<std::vector<T>> data;

    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line)) {
            std::istringstream iss(line);
            std::vector<T> row;
            std::string str_line;

            while (std::getline(iss, str_line, ',')) {
                try {
                    double value = std::stod(str_line);  // Convert token to double
                    row.push_back(value);
                } catch (const std::invalid_argument& e) {
                } catch (const std::out_of_range&) {
                    row.push_back(T{});
                }
            }

            if (!row.empty()) {
                data.push_back(row);
            }
        }
        file.close();
    }
    return data;
}

template <typename T>
inline std::vector<std::vector<T>> order_data(std::vector<std::vector<T>> data, int idx1 = -1, int idx2 = -1,
                                              int idx3 = -1, int idx4 = -1) {
    if (idx1 == -1) return data;

    auto validate_index = [&](int idx) {
        if (idx != -1 && (idx < 0 || idx >= static_cast<int>(data.front().size()))) {
            throw std::out_of_range("Index out of range: " + std::to_string(idx));
        }
    };

    validate_index(idx1);
    validate_index(idx2);
    validate_index(idx3);
    validate_index(idx4);

    if (idx2 == -1 && idx3 == -1 && idx4 == -1) {
        std::sort(data.begin(), data.end(), [idx1](const std::vector<T>& a, const std::vector<T>& b) {
            return std::tie(a[idx1]) < std::tie(b[idx1]);
        });
    } else if (idx3 == -1 && idx4 == -1) {
        std::sort(data.begin(), data.end(), [idx1, idx2](const std::vector<T>& a, const std::vector<T>& b) {
            return std::tie(a[idx1], a[idx2]) < std::tie(b[idx1], b[idx2]);
        });
    } else if (idx4 == -1) {
        std::sort(data.begin(), data.end(), [idx1, idx2, idx3](const std::vector<T>& a, const std::vector<T>& b) {
            return std::tie(a[idx1], a[idx2], a[idx3]) < std::tie(b[idx1], b[idx2], b[idx3]);
        });
    } else {
        std::sort(data.begin(), data.end(), [idx1, idx2, idx3, idx4](const std::vector<T>& a, const std::vector<T>& b) {
            return std::tie(a[idx1], a[idx2], a[idx3], a[idx4]) < std::tie(b[idx1], b[idx2], b[idx3], b[idx4]);
        });
    }

    return data;
}

template <typename T>
inline std::vector<T> get_column_from_matrix(const std::vector<std::vector<T>>& matrix, size_t index) {
    std::vector<T> column;
    for (const auto& row : matrix) {
        column.push_back(row[index]);
    }
    return column;
}

template <typename T>
inline T calculate_minimum_overlap(const std::vector<T>& vec1, const std::vector<T>& vec2) {
    T min1 = *std::min_element(vec1.begin(), vec1.end());
    T min2 = *std::min_element(vec2.begin(), vec2.end());
    return std::max(min1, min2);
}

template <typename T>
inline T calculate_maximum_overlap(const std::vector<T>& vec1, const std::vector<T>& vec2) {
    T max1 = *std::max_element(vec1.begin(), vec1.end());
    T max2 = *std::max_element(vec2.begin(), vec2.end());
    return std::min(max1, max2);
}

}  // namespace Synthesis
