#include <gtest/gtest.h>
#include "base_solver.hpp"

// Define the residual function for the Gaussian model
struct GaussianCostFunction {
    GaussianCostFunction(double x, double y) : x_(x), y_(y) {}

    template <typename T>
    bool operator()(const T* const A, const T* const mu, const T* const sigma, T* residual) const {
        residual[0] = T(y_) - *A * exp(-0.5 * ((T(x_) - *mu) * (T(x_) - *mu)) / (*sigma * *sigma));
        return true;
    }

private:
    const double x_;
    const double y_;
};

class test_root_solver : public Synthesis::SynthesisMethod {
public:


    
    test_root_solver(Synthesis::InputParams &input) : SynthesisMethod(input) {}

    ceres::TerminationType use_root_solver(double x, double y, double* solution) {
        setup_solver_options();
        ceres::Problem problem;

        // Generate synthetic data
        std::vector<double> y_data = {1.0, 2.0, 3.0, 2.0, 1.0};
        std::vector<double> x_data = {0.0, 1.0, 2.0, 3.0, 4.0};

        // Initialize parameters
        double A = 1.0;
        double mu = 2.0;
        double sigma = 1.0;

        problem.AddParameterBlock(&A, 1);
        problem.AddParameterBlock(&mu, 1);
        problem.AddParameterBlock(&sigma, 1);

        // Add residual blocks for each data point
        for (size_t i = 0; i < x_data.size(); ++i) {
            ceres::CostFunction* cost_function =
                new ceres::AutoDiffCostFunction<GaussianCostFunction, 1, 1, 1, 1>(new GaussianCostFunction(x_data[i], y_data[i]));
            problem.AddResidualBlock(cost_function, nullptr, &A, &mu, &sigma);
        }

        ceres::Solver::Options options;
        ceres::Solver::Summary summary;

        ceres::Solve(options, &problem, &summary);

        solution[0] = mu;
        solution[1] = sigma;

        return summary.termination_type;
    }

    void compute_synthesis() override {}
};

// Test case for Gaussian fitting using CERES solver
TEST(GaussianFittingTest, FitGaussianModel) {

    Synthesis::InputParams input_params;
    input_params.solver.parameter_tolerance= 1e-10; 
    input_params.solver.function_tolerance= 1e-10;
    input_params.solver.gradient_tolerance= 1e-12;
    input_params.solver.use_nonmonotonic_steps= true;
    input_params.solver.max_iterations= 1e4;

    input_params.solver.method = "levenbergMarquardt";
    input_params.solver.linear_solver = "denseQR";
    input_params.verbose= 2;
    test_root_solver test_solver(input_params);
    double x = 1.0;
    double y = 2.0;
    double result[2];

    ceres::TerminationType solver_convergence = test_solver.use_root_solver(x, y, result);

    EXPECT_EQ(solver_convergence, ceres::CONVERGENCE);
    EXPECT_NEAR(result[0], 2.0, 1e-10);
    // Add assertion for sigma if needed
}
