#include <gtest/gtest.h>
#include "input.hpp"
#include "output.hpp"
#include "data.hpp"


// Test case for checking if invalid YAML file path throws an exception
TEST(InputTest, InvalidYAMLFilePath) {
    std::string config_path = "invalid/path/to/config.yaml"; // Provide an invalid path
    // Check if attempting to create Input object with invalid path throws an exception
    
    ASSERT_THROW(Synthesis::Input input(config_path), std::runtime_error);
}

// Test case for opening a file
TEST(OutputTest, OpenFile) {

    Synthesis::OutputHandler output;
    
    //Create output directory
    std::string output_path = "../output/test_run/";
    bool result_mkdir = mkdir(output_path.c_str(), 0755);
    ASSERT_TRUE(result_mkdir == 0 || errno == EEXIST);

    std::string filename = "test_output.csv";
    output.open_file(output_path+filename);
    ASSERT_TRUE(output.is_open());
    output.close_file(); // Close the file after the test
}

// Test case for writing a single data item
TEST(InputOutputTest, WriteSingleDataItem) {
    Synthesis::OutputHandler output;
    std::string output_path = "../output/test_run/test_output.csv";
    
    output.open_file(output_path);

    double data = 42.0;
    output.write(data, false);
    output.close_file();

    // Now, read the file and check if the data is written correctly
    std::ifstream infile(output_path);
    double read_data;   
    infile >> read_data;
    infile.close();
    ASSERT_NEAR(data, read_data, 1e-6);
}

// Test case for writing multiple data items
TEST(InputOutputTest, WriteAndReadMultipleDataItems) {

    std::vector<std::vector<double>> data = {{1., 2., 3., 4., 5.}, {6., 7., 8., 9., 10.}};
    Synthesis::OutputHandler output;
    std::string output_path = "../output/test_run/test_output.csv";

    output.open_file(output_path);

    output.write(data[0]);    
    output.skip_line();
    output.write(data[1]);

    output.close_file();

    // Now, read the file and check if the data is written correctly
    Synthesis::DataContainer<double> input_data;
    input_data.read(output_path);

    ASSERT_EQ(data, input_data.get_data());
}


