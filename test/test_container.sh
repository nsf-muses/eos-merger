#!/bin/bash

# Exit on error
set -euo pipefail

echo -e "Test Docker container"

# Get the path of the script
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

# Change to the parent directory
cd $SCRIPT_PATH/..

# Build the docker image
docker build -t synthesis:dev .

# Create output directory if it doesn't exist
if [[ ! -d "output" ]]; then
    mkdir -p output
fi

# Run the docker image
docker run -it --rm \
    synthesis:dev  sh -c "cd /opt/test/ && bash test_all.sh"


echo  -e "\n\tDocker test: OK\n"

exit 0