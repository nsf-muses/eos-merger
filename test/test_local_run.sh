#!/bin/bash

# Exit on error
set -euo pipefail

RUN_NAME="example"

# Get the path of the script
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"

# Change to the parent directory
cd $SCRIPT_PATH/..

# Check if manifest.yaml exists
if [[ ! -f "manifest.yaml" ]]; then
    echo "Error: manifest.yaml not found. Aborting."
    exit 1
fi

#Go to source code
cd src
mkdir -p ../input/test

#generate the input files
python3 create_config.py   --run_name="${RUN_NAME}" \
                            --config_file_path="../input/test/"\
                            --synthesis_type=1 \
                            --verbose=0 \
                            --eos_input_file ../input/test/cmf_baryons/eos.csv \
                                              ../input/test/cmf_quarks/eos.csv

#validate the input files
python3 validate_config.py   --config_file_path="../input/test/" --output_config_file_path="../input/test/"

#compile and run the synthesis
./synthesis test

echo -e "\\n\\n See output at eos-synthesis/output/${RUN_NAME}"
# rm ../input/test_config.yaml ../input/validated_example_config.yaml


exit 0
