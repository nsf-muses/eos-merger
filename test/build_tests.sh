#!/bin/bash
set -euo pipefail

source_dir="$(dirname "$(readlink -f "$0")")/../src/"

# Check if the source directory exists
if [ ! -d "$source_dir" ]; then
  echo "Directory not found: $source_dir"
  exit 1
fi

# Function to build a unit test
build_test() {
    local test_name="$1"

    echo -e "\nBuilding unit tests: $test_name"
    
    g++ $test_name.cpp \
        -I$source_dir -Wall \
        -I/usr/local/include \
        -I/usr/include/eigen3 \
        -std=gnu++2a \
        -o $test_name \
        -lyaml-cpp -lm -lceres -lglog -lgsl -lgslcblas -lgmp -lgtest -lgtest_main
    
    echo -e "Built test: $test_name.cpp -> $test_name"   
}

echo -e "\nBuilding unit tests..."

# Build each unit test individually
for file in ./*.cpp; do build_test $(basename "${file%.*}"); done;

echo -e "\nFinished building unit tests.\n"
