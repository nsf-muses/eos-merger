#!/usr/bin/bash

# Run tests to test Lepton module functionalities

SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
CURRENT_DIR=$(pwd)

# Change to the script directory if we are not already there
if [ "$SCRIPT_PATH" != "$CURRENT_DIR" ]; then
    cd "$SCRIPT_PATH"
fi

echo -e "Running all tests\n" 

bash test_yaml.sh

bash run_tests.sh

echo "Tests Completed"

exit 0