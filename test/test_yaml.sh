#!/usr/bin/bash

# Test YAML validator

set -e

# Get the path of the script and of the current directory
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
CURRENT_DIR=$(pwd)

# Change to the script directory if we are not already there
if [ "$SCRIPT_PATH" != "$CURRENT_DIR" ]; then
    cd "$SCRIPT_PATH"
fi

echo -e "Test YAML generator & validation script"
cd ../src/
python3 create_config.py    --config_file_path="../input/test/"\
                            > /dev/null

python3 validate_config.py --config_file_path=../input/test/  --output_config_file_path=../input/test/ \
                                   > /dev/null

echo  -e "\n\tYAML validation: OK\n"

exit 0