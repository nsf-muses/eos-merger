#!/bin/bash

set -uo pipefail


# Function to run a unit test
run_test() {
  local test_name="$1"

  echo -e "\nRunning unit test: $test_name"
  "./${test_name}"
  local exit_code=$?
  (( RESULT += exit_code ))
}

echo -e "\nRunning unit tests..."
RESULT=0

# Run each unit test individually
for file in ./*.cpp; do run_test $(basename "${file%.*}"); done;

if [ $RESULT -eq 0 ]; then
  echo  -e "\n\tC++ test: OK\n"
else
  echo  -e "\n\tC++ test: Failed\n"
  RESULT=1
fi

exit $RESULT