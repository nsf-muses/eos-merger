#include <gtest/gtest.h>
#include "interpolator.hpp"

// Test case for normal interpolation
TEST(InterpolatorTest, Interpolate1D_Normal) {
    // Arrange
    Synthesis::Interpolator interpolator;
    std::vector<std::vector<double>> data = {{1., 10.0, 30.}, {2.0, 20.0, 60.}, {3.0, 30.0, 90.}, {4., 40.0, 120.}, {5., 50.0, 150.}};
    interpolator.set_1d_interpolation(data, 0);

    // Act
    double x = 2.5;
    double interpolated_y = interpolator.interpolate_1d(x, 1);
    double interpolated_z = interpolator.interpolate_1d(x, 2);
    
    // Assert
    ASSERT_DOUBLE_EQ(interpolated_y, 25.0);
    ASSERT_DOUBLE_EQ(interpolated_z, 75.0);
    ASSERT_TRUE(std::isinf(interpolator.interpolate_1d(0.9, 1)));
}

TEST(InterpolatorTest, Interpolate1D_OutOfBounds) {
    // Arrange
    Synthesis::Interpolator interpolator;
    std::vector<std::vector<double>> data = {{1., 10.0, 30.}, {2.0, 20.0, 60.}, {3.0, 30.0, 90.}, {4., 40.0, 120.}, {5., 50.0, 150.}};
    interpolator.set_1d_interpolation(data, 0);

    // Act & Assert
    ASSERT_FALSE(interpolator.is_value_in_interpolator_1d(10.0));
    ASSERT_TRUE(interpolator.is_value_in_interpolator_1d(4.5));
}


// Test case for resetting 1D interpolation
TEST(InterpolatorTest, Reset1DInterpolation) {
    Synthesis::Interpolator interpolator;
    std::vector<std::vector<double>> data = {{1., 10.0, 30.}, {2.0, 20.0, 60.}, {3.0, 30.0, 90.}, {4., 40.0, 120.}, {5., 50.0, 150.}};
    interpolator.set_1d_interpolation(data, 0);

    // Act & Assert
    interpolator.reset_1d_interpolation();

    // Check if the xv and func vectors are cleared
    ASSERT_TRUE(interpolator.get_x_vector().empty());
    ASSERT_TRUE(interpolator.get_func().empty());
}


TEST(InterpolatorTest, Interpolate2D_Normal) {
    // Arrange
    Synthesis::Interpolator interpolator;
    std::vector<std::vector<double>> data = 
    {       {1.0, 2.0, 10.0, 30.},  {2.0, 2.0, 30.0, 30.},  {3.0, 2.0, 50.0, 30.},  {4.0, 2.0, 70.0, 30.}, 
            {1.0, 3.0, 20.0, 60.},  {2.0, 3.0, 20.0, 60.},  {3.0, 3.0, 20.0, 60.},  {4.0, 3.0, 20.0, 60.},
            {1.0, 4.0, 30.0, 90.},  {2.0, 4.0, 30.0, 90.},  {3.0, 4.0, 30.0, 90.},  {4.0, 4.0, 30.0, 90.},
            {1.0, 5.0, 40.0, 120.}, {2.0, 5.0, 40.0, 120.}, {3.0, 5.0, 40.0, 120.}, {4.0, 5.0, 40.0, 120.},
            {1.0, 6.0, 50.0, 150.}, {2.0, 6.0, 50.0, 150.}, {3.0, 6.0, 50.0, 150.}, {4.0, 6.0, 50.0, 150.}};
    interpolator.set_2d_interpolation(data, 0, 1); // x and y coordinates are the first two columns


    double x = 1.5;
    double y = 2.5;
    double z_expected_value = 25.0;
    double w_expected_value = 45.0;

    double z_interpolated_value = interpolator.interpolate_2d(x, y, 2); // Interpolate third column 
    double w_interpolated_value = interpolator.interpolate_2d(x, y, 3); // Interpolate fourth interpolate
    // Assert
    ASSERT_DOUBLE_EQ(z_interpolated_value, z_expected_value);
    ASSERT_DOUBLE_EQ(w_interpolated_value, w_expected_value);
}

TEST(InterpolatorTest, Interpolate2D_OutOfBounds) {
    // Arrange
    Synthesis::Interpolator interpolator;
    std::vector<std::vector<double>> data = 
    {       {1.0, 2.0, 10.0, 30.},  {2.0, 2.0, 30.0, 30.},  {3.0, 2.0, 50.0, 30.},  {4.0, 2.0, 70.0, 30.}, 
            {1.0, 3.0, 20.0, 60.},  {2.0, 3.0, 20.0, 60.},  {3.0, 3.0, 20.0, 60.},  {4.0, 3.0, 20.0, 60.},
            {1.0, 4.0, 30.0, 90.},  {2.0, 4.0, 30.0, 90.},  {3.0, 4.0, 30.0, 90.},  {4.0, 4.0, 30.0, 90.},
            {1.0, 5.0, 40.0, 120.}, {2.0, 5.0, 40.0, 120.}, {3.0, 5.0, 40.0, 120.}, {4.0, 5.0, 40.0, 120.},
            {1.0, 6.0, 50.0, 150.}, {2.0, 6.0, 50.0, 150.}, {3.0, 6.0, 50.0, 150.}, {4.0, 6.0, 50.0, 150.}};
    interpolator.set_2d_interpolation(data, 0, 1);

    // Act & Assert
    ASSERT_FALSE(interpolator.is_value_in_interpolator_2d(0.5, 2.5)); // Point outside the interpolation range
    ASSERT_TRUE(interpolator.is_value_in_interpolator_2d(1.5, 2.5)); // Point inside the interpolation range
}


// Test case for resetting 2D interpolation
TEST(InterpolatorTest, Interpolate2d_OutOfBounds) {
    Synthesis::Interpolator interpolator;
    std::vector<std::vector<double>> data = 
    {       {1.0, 2.0, 10.0, 30.},  {2.0, 2.0, 30.0, 30.},  {3.0, 2.0, 50.0, 30.},  {4.0, 2.0, 70.0, 30.}, 
            {1.0, 3.0, 20.0, 60.},  {2.0, 3.0, 20.0, 60.},  {3.0, 3.0, 20.0, 60.},  {4.0, 3.0, 20.0, 60.},
            {1.0, 4.0, 30.0, 90.},  {2.0, 4.0, 30.0, 90.},  {3.0, 4.0, 30.0, 90.},  {4.0, 4.0, 30.0, 90.},
            {1.0, 5.0, 40.0, 120.}, {2.0, 5.0, 40.0, 120.}, {3.0, 5.0, 40.0, 120.}, {4.0, 5.0, 40.0, 120.},
            {1.0, 6.0, 50.0, 150.}, {2.0, 6.0, 50.0, 150.}, {3.0, 6.0, 50.0, 150.}, {4.0, 6.0, 50.0, 150.}};
    interpolator.set_2d_interpolation(data, 0, 1);

    ASSERT_FALSE(interpolator.get_function_2d().empty());
    ASSERT_FALSE(interpolator.get_points_2d().empty());
    // Act & Assert
    interpolator.reset_2d_interpolation();

    // Check if the xv and func vectors are cleared
    ASSERT_TRUE(interpolator.get_function_2d().empty());
    ASSERT_TRUE(interpolator.get_points_2d().empty());

}