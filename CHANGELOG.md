# Changelog


## v1.0.0
 - Update for beta-release of Calculation Engine

## v0.8.6 

### Fixed
  - Fixed particle properties output at the transition point. Now quantities other than temperature, baryon chemical potential and baryon density are set to zero at the transition, to mark the discontinuity in the plots.

## v0.8.5

### Fixed
- Fixed throw message shown in the try-catch chain.
- Fixed status code and message when a failure occurs.

### Added
-  User can choose CERES Solver options in the config.yaml.

## v0.8.4 

### Fixed
- Added the transition point to the outputted EoS. Previously, only the first EoS was included.
- Added a small dP to the pressure of the second EoS at the transition point to ensure QLIMR does not raise errors about stability.
- Removed extra speed of sound column from the eos output file for the speed of sound interpolation.

### Changed
- Updated how derivatives are calculated in the Maxwell phase transition. Now it uses the derivatives of the two input EoS and attaches them at the phase transition point.

## v0.8.3
### Fixed
- Fixed an issue where outputs had multiple temperature values depending on the initial EoS, which broke CE workflows.

### Changed
- Updated error messages in case of failure.

## v0.8.2 
### Fixed
- Corrected the ordering of the reduced (original) EoS in the $P(\mu_B)$ interpolation to follow the baryon density instead of the chemical potential.

## v0.8.1
### Fixed
- Ensured the status file is created if the `config.yaml` validation fails in `validate_config.py`.

## v0.8.0
### Added
- Started a Changelog.
- Added option to interpolate between the speed of sound as a function of baryon density using the hyperbolic tangent method.
- Introduced option to compute derivatives with GSL.
- Added configuration parameters to derivatives key [Breaking change - Users need to update their configurations]:
    - Option to compute derivatives via either finite difference method or automatically compute it with GSL.
    - Added finite difference subkey to derivatives key:
        - Added option to choose between relative or absolute steps in the finite difference derivative method
- Improved stability and causality checks: if the options check_eos_stability and check_eos_causality are enabled, the code will not only check the stability and causality conditions, but also throw an error if the EoS is unstable or not causal.

### Refactored
- Refactored code to use polymorphism for better maintainability.