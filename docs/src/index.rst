Synthesis Module
================

The Synthesis module is part of the `MUSES <https://musesframework.io/>`_ framework, and it is designed to be used with an input configuration file (the `config.yaml <https://gitlab.com/nsf-muses/eos-synthesis/-/blob/main/input/config.yaml?ref_type=heads>`_). 

The code provides methods for joining two one-dimensional Equations of State (EoS), the options  available are:

- Maxwell phase transition (PT)
- Gibbs phase transition
- Hyperbolic tangent interpolation 
- Attaching the two EoS at a user defined variable and value.
 
.. toctree::
   :maxdepth: 2
   :caption: Contents
   :glob:

   _contents/quickstart.rst
   _contents/physics.rst

   Input <_contents/input/index.rst>
   Output <_contents/output/index.rst>


More information about the module input and output can be found in the `OpenAPI Specifications <https://gitlab.com/nsf-muses/eos-synthesis/-/blob/dev/api/OpenAPI_Specifications_synthesis.yaml?ref_type=heads>`_.

Changes from past versions can be found in the `CHANGELOG <https://gitlab.com/nsf-muses/eos-synthesis/-/blob/dev/CHANGELOG.md?ref_type=heads>`_.