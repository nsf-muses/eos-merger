Physics
=======================


At the current moment, the module supports:

Maxwell phase transition
------------------------

The Maxwell phase transition requires only two EoS files, specified via the **eos_input_file** options under the `model1_BetaEq_files` and `model2_BetaEq_files` sections. This type of phase transition (PT) occurs when the baryon chemical potentials and pressures cross:

.. math::

    \mu_B^I = \mu_B^{II} \qquad P^I = P^{II}

While these quantities are continuous, others such as the baryon density (:math:`n_B`), energy density (:math:`\varepsilon`), charge chemical potential (:math:`\mu_Q`), and charge density (:math:`n_Q`) are discontinuous. 
To use this phase transition, set the option **synthesis_type** to *'maxwell'*.

Gibbs phase transition
----------------------

The Gibbs phase transition requires two beta-equilibrated EoS files, the pure leptonic EoS, and the two gridded EoS files without leptons (i.e., the EoS files that come directly from the Nuclear physics module). \
All of these are specified in the **eos_input_file** options. This type of PT occurs over a certain range of densities, where phase I occupies a volume fraction :math:`f` and phase II occupies :math:`1-f`. In this case, pressure, baryon chemical potential, and charge chemical potentials match:

.. math::

    \mu_B^I = \mu_B^{II} \qquad \mu_Q^I = \mu_Q^{II} \qquad P^I = P^{II}

and the net electric charge must be zero:

.. math::

    f n_Q^I + (1-f) n_Q^{II} + n_Q^{leptons} = 0

To use this phase transition, set the option **synthesis_type** to *'gibbs'*.

Hyperbolic tangent interpolation
--------------------------------

The interpolation is a well-known method for joining different EoS's by interpolating them. In this module, the hyperbolic tangent method is used, where the interpolating functions are defined as:

.. math::

    f_\pm(x) = \frac{1}{2} \left[ 1 \pm \tanh \left( \frac{x - \bar{x}}{\Gamma} \right) \right],

where :math:`x` is some thermodynamic variable, :math:`\bar{x}` is the midpoint of the merging, and :math:`\Gamma` is its width. 
The function :math:`Y(x)` to be interpolated can be chosen and it is defined as:

.. math::

     Y(x) = Y^I f_- (x) + Y^{II} f_+ (x)

The options for the variables :math:`Y(x)` are the following:

.. math::

    \varepsilon(n_B), \quad P(n_B), \quad P(\mu_B), c_s^2(n_B)

Other thermodynamic quantities are obtained from standard thermodynamic relations, such as 

.. math:: 

    P= n_B^2 \frac{\partial \varepsilon/n_B}{\partial n_B} \text{,} \quad n_B = \frac{\partial P}{\partial \mu_B} \text{and} 
    \mu_B= \frac{\varepsilon+ P}{n_B}


except in the speed of sound interpolation, where we integrate the EoS upwards, using

.. math::

    n_{B, i+1} = n_{B, i} + \Delta n_B

.. math::

    \varepsilon_{i+1} = \varepsilon_i + \Delta n_B \left( \frac{\varepsilon_i + P_i}{n_{B, i}} \right)

.. math::

    P_{i+1} = P_i + c_s^2(n_{B, i}) \Delta \varepsilon

following arXiV:2106.03890.

To use the interpolation, set **synthesis_type** to *'hyperbolic-tangent'* in the config file, and use the variables in the **interpolation_method** section. 
The option **use_thermodynamic_consistency** is enabled by default, and it is highly recommended.  
If it is disabled by the user, all thermodynamic variables will be obtained just like :math:`Y(x)`, instead of using the thermodynamic relations.


Attachment method
-----------------

To simply attach the EoS, set **synthesis_type** to '*attach*' and define the **attach_variable** and **attach_value**. For example, if the user chooses **attach_variable=pressure** and **attach_value=10**, then the output will contain the first EoS at :math:`P < 10` MeV fm\ :sup:`-3` and the second EoS at :math:`P > 10` MeV fm\ :sup:`-3`.