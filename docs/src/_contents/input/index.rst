Input
=============

The configuration file is the main input file for the Synthesis module, and is written in YAML format. 
All the input data files **must** be in `.csv` format. 
The beta-equilibrated Equations of State are always required. 
For the Gibbs phase transition, the 2-dimensional grid files and a leptonic EoS file provided by the `Lepton module <https://gitlab.com/nsf-muses/module-cmf/lepton-module>`_ must also be provided.
 
The input files are:

.. toctree::
   :maxdepth: 1

   config.rst
   eos.rst
   particle.rst
