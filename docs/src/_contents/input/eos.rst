Equation of State
=====================

The beta-equilibrated nuclear EoS' input files **are required**. 
It must contain the following columns, presented in order:

+----------------------------------------------------+------------------+
| **Variable**                                       | **Unit**         |
+====================================================+==================+
| Temperature (T)                                    | MeV              |
+----------------------------------------------------+------------------+
| Baryon chemical potential (:math:`\mu_B`)          | MeV              |
+----------------------------------------------------+------------------+
| Strange chemical potential (:math:`\mu_S`)         | MeV              |
+----------------------------------------------------+------------------+
| Electric charge chemical potential (:math:`\mu_Q`) | MeV              |
+----------------------------------------------------+------------------+
| Baryon density (:math:`n_B`)                       | fm\ :sup:`-3`    |
+----------------------------------------------------+------------------+
| Strange density (:math:`n_S`)                      | fm\ :sup:`-3`    |
+----------------------------------------------------+------------------+
| Electric charge density (:math:`n_Q`)              | fm\ :sup:`-3`    |
+----------------------------------------------------+------------------+
| Energy density (:math:`\varepsilon`)               | MeV/fm\ :sup:`3` |
+----------------------------------------------------+------------------+
| Pressure (:math:`P`)                               | MeV/fm\ :sup:`3` |
+----------------------------------------------------+------------------+
| Entropy density (:math:`s`)                        | 1/fm\ :sup:`3`   |
+----------------------------------------------------+------------------+

The grid nuclear EoS' and the Lepton EoS input files are required **only** for the Gibbs phase transition. The grids follow the same format as the beta-equilibrated nuclear EoS, and the lepton EoS file must contain the following columns:

+----------------------------------------------------+------------------+
| **Variable**                                       | **Unit**         |
+====================================================+==================+
| Temperature (T)                                    | MeV              |
+----------------------------------------------------+------------------+
| Electron chemical potential (:math:`\mu_e`)        | MeV              |
+----------------------------------------------------+------------------+
| Neutrino chemical potential (:math:`\mu_\nu`)      | MeV              |
+----------------------------------------------------+------------------+
| Lepton density (:math:`n_L`)                       | fm\ :sup:`-3`    |
+----------------------------------------------------+------------------+
| Electric charge density (:math:`n_Q`)              | fm\ :sup:`-3`    |
+----------------------------------------------------+------------------+
| Energy density (:math:`\varepsilon`)               | MeV/fm\ :sup:`3` |
+----------------------------------------------------+------------------+
| Pressure (:math:`P`)                               | MeV/fm\ :sup:`3` |
+----------------------------------------------------+------------------+
| Entropy density (:math:`s`)                        | 1/fm\ :sup:`3`   |
+----------------------------------------------------+------------------+

