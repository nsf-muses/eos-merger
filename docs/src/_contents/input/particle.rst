Particle Properties
=====================

The particle properties file is **optional**.
Provide the path using the **particle_properties_input_file** if you are not using the Calculation Engine, and enable the **output_particle_properties** option in the config file to generate its output.

The only requirements is that the first 7 columns must be the same as the EoS file, namely :math:`(T, \mu_B, \mu_S, \mu_Q, n_B, n_S, n_Q)`.
In this way, the file can be interpolated for the beta-equilibrated EoS.
Further columns can be added to have your own model information, such as particle densities, chemical potentials, masses, meson field values, baryon number, etc. 

For the particle properties file associated to the Leptonic EoS, the format follows the Lepton module standard:

+------------------------------------------------------------+------------------+
| **Variable**                                               | **Unit**         |
+============================================================+==================+
| Temperature (T)                                             | MeV              |
+------------------------------------------------------------+------------------+
| Lepton chemical potential (:math:`\mu_\text{leptons}`)      | MeV              |
+------------------------------------------------------------+------------------+
| Neutrino chemical potential (:math:`\mu_\text{neutrino}`)   | MeV              |
+------------------------------------------------------------+------------------+
| Electron mass (:math:`m_e`)                                 | MeV              |
+------------------------------------------------------------+------------------+
| Muon mass (:math:`m_\mu`)                                   | MeV              |
+------------------------------------------------------------+------------------+
| Tau mass (:math:`m_\tau`)                                   | MeV              |
+------------------------------------------------------------+------------------+
| Electron neutrino mass (:math:`m_{\nu_e}`)                  | MeV              |
+------------------------------------------------------------+------------------+
| Muon neutrino mass (:math:`m_{\nu_\mu}`)                    | MeV              |
+------------------------------------------------------------+------------------+
| Tau neutrino mass (:math:`m_{\nu_\tau}`)                    | MeV              |
+------------------------------------------------------------+------------------+
| Electron chemical potential (:math:`\mu_e`)                 | MeV              |
+------------------------------------------------------------+------------------+
| Muon chemical potential (:math:`\mu_\mu`)                   | MeV              |
+------------------------------------------------------------+------------------+
| Tau chemical potential (:math:`\mu_\tau`)                   | MeV              |
+------------------------------------------------------------+------------------+
| Electron neutrino chemical potential (:math:`\mu_{\nu_e}`)  | MeV              |
+------------------------------------------------------------+------------------+
| Muon neutrino chemical potential (:math:`\mu_{\nu_\mu}`)    | MeV              |
+------------------------------------------------------------+------------------+
| Tau neutrino chemical potential (:math:`\mu_{\nu_\tau}`)    | MeV              |
+------------------------------------------------------------+------------------+
| Electron density (:math:`n_e`)                              | fm\ :sup:`-3`    |
+------------------------------------------------------------+------------------+
| Muon density (:math:`n_\mu`)                                | fm\ :sup:`-3`    |
+------------------------------------------------------------+------------------+
| Tau density (:math:`n_\tau`)                                | fm\ :sup:`-3`    |
+------------------------------------------------------------+------------------+
| Electron neutrino density (:math:`n_{\nu_e}`)               | fm\ :sup:`-3`    |
+------------------------------------------------------------+------------------+
| Muon neutrino density (:math:`n_{\nu_\mu}`)                 | fm\ :sup:`-3`    |
+------------------------------------------------------------+------------------+
| Tau neutrino density (:math:`n_{\nu_\tau}`)                 | fm\ :sup:`-3`    |
+------------------------------------------------------------+------------------+
