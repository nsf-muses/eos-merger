Derivatives
=====================

The derivatives output file will be provided if the options **output_derivatives** and the  **use_beta_equilibrium** options are enabled. 
It contains the columns below:

+----------------------------------------------------+---------------+
| **Variable**                                       | **Unit**      |
+====================================================+===============+
| Temperature (T)                                    | MeV           |
+----------------------------------------------------+---------------+
| Baryon chemical potential (:math:`\mu_B`)          | MeV           |
+----------------------------------------------------+---------------+
| Strange chemical potential (:math:`\mu_S`)         | MeV           |
+----------------------------------------------------+---------------+
| Electric charge chemical potential (:math:`\mu_Q`) | MeV           |
+----------------------------------------------------+---------------+
| Baryon density (:math:`n_B`)                       | fm\ :sup:`-3` |
+----------------------------------------------------+---------------+
| Strange density (:math:`n_S`)                      | fm\ :sup:`-3` |
+----------------------------------------------------+---------------+
| Electric charge density (:math:`n_Q`)              | fm\ :sup:`-3` |
+----------------------------------------------------+---------------+
| Speed of sound squared (:math:`c_s^2`)             |  c            |
+----------------------------------------------------+---------------+
| 1st baryon susceptibility (:math:`\chi_{B1}`)      | fm\ :sup:`-3` |
+----------------------------------------------------+---------------+
| 2nd baryon susceptibility (:math:`\chi_{B2}`)      | MeV :sup:`2`  |
+----------------------------------------------------+---------------+
| 3rd baryon susceptibility (:math:`\chi_{B3}`)      | MeV           |
+----------------------------------------------------+---------------+


The baryon successibilities are defined as 

.. math:: 
   \chi_{Bn} = \frac{\partial^n P}{\partial \mu_B^n}, 

and the speed of sound squared is defined as 

.. math:: 
   c_s^2 = \frac{\partial P}{\partial \epsilon},


and is in units of the speed of light squared.



