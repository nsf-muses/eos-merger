Particle Properties
=====================

The particle properties input file is **optional**. Provide the path using the **particle_properties_input_file** if you are not using the Calculation Engine, and enable the **output_particle_properties** option in the config file to generate its output.

Since this file does not have requirements, the output will have the same columns as the input file.

**Important**: Make sure the columns in the two particle properties files are the same, as the module will not check for this, and it will interpolate between the two files.

