Output
=============

All outputs are in `.csv` format by default. You can have the EoS and the derivatives file in HDF5  format by setting **output_hdf5: true** in the `config.yaml` file.
You can also have the EoS in compOSE format by setting **output_compOSE: true** in the `config.yaml` file.

The method to generating the output files depend on the type of synthesis done. The generated files are:

.. toctree::
   :maxdepth: 1

   eos.rst
   particle.rst
   derivatives.rst
