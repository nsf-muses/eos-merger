Quickstart
==========


The code can be used locally, in a Docker image, or in the calculation engine. The following sections will explain the first two cases.
For the calculation engine, please check the `Tutorial <https://musesframework.io/docs/user/tutorial/Readme.html>`_.

Local Usage
~~~~~~~~~~

To download the latest version of the module, type::

    git clone https://gitlab.com/nsf-muses/module-cmf/eos-synthesis


Required Libraries
------------------

The following libraries are required:

- **ceres-solver**: `Ceres <http://ceres-solver.org>`_ is a C++ library for solving large-scale optimization problems.
  
  - **Linux**: Install using your package manager. For Debian, use:

    .. code-block:: bash

        sudo apt-get install libceres-dev

  - **macOS**: Install using Homebrew:

    .. code-block:: bash

        brew install ceres-solver

  - **Windows**: Use `Cygwin <https://www.cygwin.com/>`_ and follow the steps `here <http://ceres-solver.org/installation.html#windows>`_.

- **yaml-cpp**: The `YAML-cpp <https://github.com/jbeder/yaml-cpp>`_ library is a YAML parser and emitter for C++.
  
  - **Linux**: Install using your package manager. For Debian, use:

    .. code-block:: bash

        sudo apt-get install libyaml-cpp-dev

  - **macOS**: Install using Homebrew:

    .. code-block:: bash

        brew install gsl

  - **Windows**: Use `Cygwin <https://www.cygwin.com/>`_, install CMake, and build yaml-cpp from source:

    .. code-block:: bash

        git clone https://github.com/jbeder/yaml-cpp.git
        cd yaml-cpp
        mkdir build && cd build
        cmake -DYAML_BUILD_SHARED_LIBS=OFF ..
        make
        make DESTDIR=/desired/path/to/installation install

    After successful compilation, the ``libyaml-cpp.a`` file will be created in DESTDIR.

- **CGAL**: `CGAL <https://www.cgal.org/>`_ is a software library for computational geometry, which we use for multidimensional interpolation.
  
  - **Linux**: Install using your package manager. For Debian, use:

    .. code-block:: bash

        sudo apt-get install libcgal-dev

  - **macOS**: Install using Homebrew:

    .. code-block:: bash

        brew install cgal

  - **Windows**: Install using `vcpkg <https://vcpkg.io/en/>`_:

    .. code-block:: bash

        vcpkg install cgal

- **Cubature**:  The `Cubature <https://github.com/stevengj/cubature>`_ is used for integrations. We adapted it to be a header-only file and it is automatically included in the module.


Python dependencies and additional MUSES libraries such as `openapi-core`, `pyyaml`, `pandas`, `MUSES porter`, and `MUSES comPOSE` can be installed by running:

.. code-block:: bash

    pip install -r requirements.txt

Compilation and Testing
----------------------


Before compiling the code, run tests to ensure all dependencies are correctly installed:

.. code-block:: bash

    cd test
    bash test_all.sh

If all tests pass, return to the source directory and compile the code:

.. code-block:: bash

    cd ../src
    make

The executable *synthesis* will be created upon successful compilation.

Running the Module
------------------

1. **Set Up Configuration File**: Create the `config.yaml` file using:

   .. code-block:: bash

       python3 create_config.py --option1 value1 --option2 value2

   For available options, refer to the `Open API Specification`_.

2. **Validate Configuration File**: Validate and fill in missing variables with default values:

   .. code-block:: bash

       python3 validate_config.py

3. **Run the C++ Code**: Run the executable with:

   .. code-block:: bash

       ./synthesis

   If using custom paths, adjust the commands as needed.

4. **Postprocessing**: Convert outputs to HDF5 format or other formats:

   .. code-block:: bash

       python3 postprocess.py

Docker Container
~~~~~~~~~~~~~~~~

To use the Synthesis module via Docker, pull the released image:

.. code-block:: bash

    docker pull registry.gitlab.com/nsf-muses/eos-synthesis:v0.7.2

Create directories for input and output files:

.. code-block:: bash

    mkdir -p input output

Run the Docker container with the appropriate commands to generate, validate, and run the configuration as described in the Local Usage section, such as

.. code-block:: bash
    docker run -it --rm -v input:/opt/input -v output:/opt/output registry.gitlab.com/nsf-muses/eos-synthesis:v0.7.2 ./synthesis

the -v option will mount your local input and output directories as volumes to the container. The container will run the synthesis executable and output the results to the output directory. 
Make sure the `config.yaml` is in the input directory, and run the preprcessing and postprocessing scripts as needed.
