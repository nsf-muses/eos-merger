Modular Unified Solver of the Equation of State (MUSES)
=========================================================

.. toctree::
   :maxdepth: 1
   :caption: Module Documentation
   :glob:

   module/**
